# Teen Kosmology

A module in [Kosmo format](https://lookmumnocomputer.discourse.group/t/kosmo-specification/896)
that provides a fairly general interface for the Teensy 4.1 to the modular synthesizer world.

For now, the module is in development and many parts have been tested in isolation.
Not tested are
* the buck converter (values from datasheet)
* the rotary encoders
* the linein/lineout amplifiers (simulated)
* the multiplexer with potentiometers
* the reverse polarity protection (simulated)

No software is available, as of yet, but
* the ADC works with https://github.com/labfruits/mcp320x
* the display works with https://github.com/olikraus/u8g2
* the audio shield works with https://github.com/PaulStoffregen/Audio
