EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Kosmo:Grove_1.12OLEDV2 U1
U 1 1 608AE429
P 3700 3500
F 0 "U1" H 3783 3575 50  0000 C CNN
F 1 "Grove_1.12OLEDV2" H 3783 3484 50  0000 C CNN
F 2 "" H 3700 3500 50  0001 C CNN
F 3 "" H 3700 3500 50  0001 C CNN
	1    3700 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 608AECF4
P 4150 3750
F 0 "J1" H 4230 3742 50  0000 L CNN
F 1 "Conn_01x04" H 4230 3651 50  0000 L CNN
F 2 "" H 4150 3750 50  0001 C CNN
F 3 "~" H 4150 3750 50  0001 C CNN
	1    4150 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector:DIN-5_180degree J2
U 1 1 608AFD34
P 5450 2350
F 0 "J2" H 5450 1983 50  0000 C CNN
F 1 "DIN-5_180degree" H 5450 2074 50  0000 C CNN
F 2 "" H 5450 2350 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 5450 2350 50  0001 C CNN
	1    5450 2350
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 608B1DE0
P 5450 3150
F 0 "J3" V 5322 3330 50  0000 L CNN
F 1 "Conn_01x03" V 5413 3330 50  0000 L CNN
F 2 "" H 5450 3150 50  0001 C CNN
F 3 "~" H 5450 3150 50  0001 C CNN
	1    5450 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	5750 2450 5750 2950
Wire Wire Line
	5750 2950 5550 2950
Wire Wire Line
	5450 2650 5450 2950
Wire Wire Line
	5350 2950 5150 2950
Wire Wire Line
	5150 2950 5150 2450
NoConn ~ 5750 2350
NoConn ~ 5150 2350
$EndSCHEMATC
