EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title ""
Date ""
Rev "0.2"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 3700 2750 0    50   Input ~ 0
Line_Out_L
Text GLabel 3700 2550 0    50   Input ~ 0
Line_Out_R
$Comp
L power:GND #PWR0111
U 1 1 61037A78
P 3100 2650
F 0 "#PWR0111" H 3100 2400 50  0001 C CNN
F 1 "GND" H 3105 2477 50  0000 C CNN
F 2 "" H 3100 2650 50  0001 C CNN
F 3 "" H 3100 2650 50  0001 C CNN
	1    3100 2650
	1    0    0    -1  
$EndComp
Text GLabel 3700 2350 0    50   Output ~ 0
Line_In_L
Text GLabel 3700 2450 0    50   Output ~ 0
Line_In_R
$Comp
L Connector_Generic:Conn_01x05 J11
U 1 1 60931B35
P 3900 2550
F 0 "J11" H 3980 2592 50  0000 L CNN
F 1 "Conn_01x05" H 3980 2501 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-05A_1x05_P2.54mm_Vertical" H 3900 2550 50  0001 C CNN
F 3 "~" H 3900 2550 50  0001 C CNN
	1    3900 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 2650 3100 2650
$EndSCHEMATC
