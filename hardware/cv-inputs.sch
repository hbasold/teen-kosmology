EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 9
Title "Teen Kosmology"
Date ""
Rev "0.2"
Comp "HB"
Comment1 "UNTESTED UNTESTED UNTESTED UNTESTED"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5800 3150 5800 3100
Wire Wire Line
	5800 3450 5800 3400
$Comp
L Device:C_Small C?
U 1 1 6017337E
P 5800 3250
AR Path="/6017337E" Ref="C?"  Part="1" 
AR Path="/6015C780/6017337E" Ref="C8"  Part="1" 
F 0 "C8" H 5708 3204 50  0000 R CNN
F 1 "0.1uF" H 5708 3295 50  0000 R CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5800 3250 50  0001 C CNN
F 3 "~" H 5800 3250 50  0001 C CNN
	1    5800 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4950 3550 4950 3650
Wire Wire Line
	4700 3450 4700 3750
Wire Wire Line
	5500 4550 5800 4550
$Comp
L power:GND #PWR?
U 1 1 6017338B
P 5800 4550
AR Path="/6017338B" Ref="#PWR?"  Part="1" 
AR Path="/6015C780/6017338B" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 5800 4300 50  0001 C CNN
F 1 "GND" H 5805 4377 50  0000 C CNN
F 2 "" H 5800 4550 50  0001 C CNN
F 3 "" H 5800 4550 50  0001 C CNN
	1    5800 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4050 5000 4050
Wire Wire Line
	4450 3950 5000 3950
Wire Wire Line
	4550 3850 5000 3850
Wire Wire Line
	4550 3350 4550 3850
Wire Wire Line
	4700 3750 5000 3750
Wire Wire Line
	4950 3650 5000 3650
$Sheet
S 3550 3950 550  300 
U 601733A2
F0 "sheet6017334D" 50
F1 "CV-input.sch" 50
F2 "ADC1" O R 4100 4050 50 
F3 "ADC2" O R 4100 4150 50 
F4 "CV1" I L 3550 4050 50 
F5 "CV2" I L 3550 4150 50 
$EndSheet
$Sheet
S 3700 4450 550  300 
U 601733A6
F0 "sheet6017334E" 50
F1 "CV-input.sch" 50
F2 "ADC1" O R 4250 4550 50 
F3 "ADC2" O R 4250 4650 50 
F4 "CV1" I L 3700 4550 50 
F5 "CV2" I L 3700 4650 50 
$EndSheet
Connection ~ 5800 4550
$Comp
L Analog_ADC:MCP3208 U?
U 1 1 601733B1
P 5600 3950
AR Path="/601733B1" Ref="U?"  Part="1" 
AR Path="/6015C780/601733B1" Ref="U4"  Part="1" 
F 0 "U4" H 5600 4631 50  0000 C CNN
F 1 "MCP3208" H 5600 4540 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 5700 4050 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21298c.pdf" H 5700 4050 50  0001 C CNN
	1    5600 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4150 5000 4150
$Comp
L power:+5V #PWR030
U 1 1 6017F161
P 6350 3150
F 0 "#PWR030" H 6350 3000 50  0001 C CNN
F 1 "+5V" H 6365 3323 50  0000 C CNN
F 2 "" H 6350 3150 50  0001 C CNN
F 3 "" H 6350 3150 50  0001 C CNN
	1    6350 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR028
U 1 1 601814B7
P 5800 3100
F 0 "#PWR028" H 5800 2850 50  0001 C CNN
F 1 "GND" H 5805 2927 50  0000 C CNN
F 2 "" H 5800 3100 50  0001 C CNN
F 3 "" H 5800 3100 50  0001 C CNN
	1    5800 3100
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 3150 6350 3400
Wire Wire Line
	6350 3400 5800 3400
Connection ~ 5800 3400
Wire Wire Line
	5800 3400 5800 3350
Text HLabel 6600 3850 2    50   Input ~ 0
CLK
Text HLabel 6600 3950 2    50   Input ~ 0
DOut
Text HLabel 6600 4050 2    50   Input ~ 0
DIn
Text HLabel 6600 4150 2    50   Input ~ 0
NegCS
Wire Wire Line
	6200 3850 6600 3850
Wire Wire Line
	6200 3950 6600 3950
Wire Wire Line
	6200 4050 6600 4050
Wire Wire Line
	6200 4150 6600 4150
Text HLabel 3500 4550 0    50   Output ~ 0
CV5
Text HLabel 3500 4650 0    50   Output ~ 0
CV6
Wire Wire Line
	3500 4650 3700 4650
Wire Wire Line
	3500 4550 3700 4550
Text HLabel 3350 4050 0    50   Output ~ 0
CV7
Text HLabel 3350 4150 0    50   Output ~ 0
CV8
Wire Wire Line
	3350 4150 3550 4150
Wire Wire Line
	3350 4050 3550 4050
$Comp
L power:+5VA #PWR029
U 1 1 61150349
P 5500 3150
F 0 "#PWR029" H 5500 3000 50  0001 C CNN
F 1 "+5VA" H 5515 3323 50  0000 C CNN
F 2 "" H 5500 3150 50  0001 C CNN
F 3 "" H 5500 3150 50  0001 C CNN
	1    5500 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3150 5500 3450
$Comp
L Device:R_POT RV?
U 1 1 60A1FADD
P 2950 850
AR Path="/6007C274/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/600880AD/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6008ABB3/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6008ACF5/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6008AD9F/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6008AE67/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6008BFD9/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6008C203/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6009C38B/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6009C458/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6009C548/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6015C780/6017339C/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733A2/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733A6/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733AA/60A1FADD" Ref="RV?"  Part="1" 
AR Path="/6015C780/60A1FADD" Ref="RV9"  Part="1" 
F 0 "RV9" V 3050 750 50  0000 R CNN
F 1 "100k" V 3150 900 50  0000 R CNN
F 2 "Kosmo_panel:Potentiometer_Alpha_RD901F-40-00D_Single_Vertical_3D" H 2950 850 50  0001 C CNN
F 3 "~" H 2950 850 50  0001 C CNN
	1    2950 850 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 700  2750 700 
Wire Wire Line
	2750 700  2750 850 
Connection ~ 2750 850 
Wire Wire Line
	2750 850  2800 850 
Wire Wire Line
	3100 850  3400 850 
Wire Wire Line
	2150 850  2750 850 
Text Notes 2150 850  0    50   ~ 0
20kΩ breakout
Wire Wire Line
	2700 900  2500 900 
Wire Wire Line
	2500 900  2500 1050
Connection ~ 2500 1050
Wire Wire Line
	2500 1050 2550 1050
Wire Wire Line
	1850 1050 2500 1050
Text Notes 1900 1050 0    50   ~ 0
20kΩ breakout
$Comp
L Device:R_POT RV?
U 1 1 60A22C08
P 2400 1350
AR Path="/6007C274/60A22C08" Ref="RV?"  Part="1" 
AR Path="/600880AD/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6008ABB3/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6008ACF5/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6008AD9F/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6008AE67/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6008BFD9/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6008C203/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6009C38B/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6009C458/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6009C548/60A22C08" Ref="RV?"  Part="1" 
AR Path="/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6015C780/6017339C/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733A2/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733A6/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733AA/60A22C08" Ref="RV?"  Part="1" 
AR Path="/6015C780/60A22C08" Ref="RV15"  Part="1" 
F 0 "RV15" H 2331 1396 50  0000 R CNN
F 1 "100k" H 2331 1305 50  0000 R CNN
F 2 "Kosmo_panel:Potentiometer_Alpha_RD901F-40-00D_Single_Vertical_3D" H 2400 1350 50  0001 C CNN
F 3 "~" H 2400 1350 50  0001 C CNN
	1    2400 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 1200 2200 1200
Wire Wire Line
	2200 1200 2200 1350
Connection ~ 2200 1350
Wire Wire Line
	2200 1350 2250 1350
Wire Wire Line
	1550 1350 2200 1350
Text Notes 1600 1350 0    50   ~ 0
20kΩ breakout
Wire Wire Line
	1900 1500 2100 1500
Wire Wire Line
	1900 1500 1900 1650
Connection ~ 1900 1650
Wire Wire Line
	1900 1650 1950 1650
Wire Wire Line
	1150 1650 1900 1650
Text Notes 1200 1650 0    50   ~ 0
20kΩ breakout
$Comp
L Device:R_POT RV?
U 1 1 60A25AA6
P 2100 1650
AR Path="/6007C274/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/600880AD/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6008ABB3/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6008ACF5/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6008AD9F/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6008AE67/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6008BFD9/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6008C203/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6009C38B/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6009C458/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6009C548/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6015C780/6017339C/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733A2/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733A6/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733AA/60A25AA6" Ref="RV?"  Part="1" 
AR Path="/6015C780/60A25AA6" Ref="RV16"  Part="1" 
F 0 "RV16" V 2000 1750 50  0000 R CNN
F 1 "100k" V 2000 1950 50  0000 R CNN
F 2 "Kosmo_panel:Potentiometer_Alpha_RD901F-40-00D_Single_Vertical_3D" H 2100 1650 50  0001 C CNN
F 3 "~" H 2100 1650 50  0001 C CNN
	1    2100 1650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR038
U 1 1 60A2B327
P 1850 2650
F 0 "#PWR038" H 1850 2400 50  0001 C CNN
F 1 "GND" H 1855 2477 50  0000 C CNN
F 2 "" H 1850 2650 50  0001 C CNN
F 3 "" H 1850 2650 50  0001 C CNN
	1    1850 2650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR037
U 1 1 60A2BA67
P 1150 2400
F 0 "#PWR037" H 1150 2250 50  0001 C CNN
F 1 "+5V" H 1165 2573 50  0000 C CNN
F 2 "" H 1150 2400 50  0001 C CNN
F 3 "" H 1150 2400 50  0001 C CNN
	1    1150 2400
	1    0    0    -1  
$EndComp
$Comp
L power:+2V5 #PWR039
U 1 1 60A2BF7F
P 2500 2400
F 0 "#PWR039" H 2500 2250 50  0001 C CNN
F 1 "+2V5" H 2515 2573 50  0000 C CNN
F 2 "" H 2500 2400 50  0001 C CNN
F 3 "" H 2500 2400 50  0001 C CNN
	1    2500 2400
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR035
U 1 1 60A2C60F
P 1300 2200
F 0 "#PWR035" H 1300 2050 50  0001 C CNN
F 1 "+12V" H 1315 2373 50  0000 C CNN
F 2 "" H 1300 2200 50  0001 C CNN
F 3 "" H 1300 2200 50  0001 C CNN
	1    1300 2200
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR036
U 1 1 60A2CC8F
P 2400 2200
F 0 "#PWR036" H 2400 2300 50  0001 C CNN
F 1 "-12V" H 2415 2373 50  0000 C CNN
F 2 "" H 2400 2200 50  0001 C CNN
F 3 "" H 2400 2200 50  0001 C CNN
	1    2400 2200
	1    0    0    -1  
$EndComp
Text HLabel 2150 850  0    50   Input ~ 0
CV1
Text HLabel 1850 1050 0    50   Input ~ 0
CV2
Text HLabel 1150 1650 0    50   Input ~ 0
CV4
Text HLabel 1550 1350 0    50   Input ~ 0
CV3
Wire Wire Line
	1600 2300 1500 2300
Wire Wire Line
	1500 2300 1500 2650
Wire Wire Line
	1500 2650 1850 2650
Wire Wire Line
	1850 2650 2200 2650
Wire Wire Line
	2200 2650 2200 2300
Wire Wire Line
	2200 2300 2100 2300
Connection ~ 1850 2650
Wire Wire Line
	2500 2400 2100 2400
$Comp
L Connector_Generic:Conn_01x04 J10
U 1 1 60AA20DC
P 4000 3450
F 0 "J10" H 3918 3025 50  0000 C CNN
F 1 "Conn_01x04" H 3918 3116 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 4000 3450 50  0001 C CNN
F 3 "~" H 4000 3450 50  0001 C CNN
	1    4000 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	4450 3950 4450 3250
Wire Wire Line
	4450 3250 4200 3250
Wire Wire Line
	4550 3350 4200 3350
Wire Wire Line
	4700 3450 4200 3450
Wire Wire Line
	4950 3550 4200 3550
Text Notes 3150 3300 0    50   ~ 0
From filter board
$Comp
L Connector_Generic:Conn_01x04 J8
U 1 1 60A1F37F
P 3600 950
F 0 "J8" H 3680 942 50  0000 L CNN
F 1 "Conn_01x04" H 3680 851 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 3600 950 50  0001 C CNN
F 3 "~" H 3600 950 50  0001 C CNN
	1    3600 950 
	1    0    0    -1  
$EndComp
Text Notes 850  700  0    50   ~ 0
To filter board
$Comp
L Device:R_POT RV?
U 1 1 60A219F1
P 2700 1050
AR Path="/6007C274/60A219F1" Ref="RV?"  Part="1" 
AR Path="/600880AD/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6008ABB3/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6008ACF5/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6008AD9F/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6008AE67/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6008BFD9/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6008C203/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6009C38B/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6009C458/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6009C548/60A219F1" Ref="RV?"  Part="1" 
AR Path="/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6015C780/6017339C/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733A2/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733A6/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6015C780/601733AA/60A219F1" Ref="RV?"  Part="1" 
AR Path="/6015C780/60A219F1" Ref="RV10"  Part="1" 
F 0 "RV10" H 2631 1096 50  0000 R CNN
F 1 "100k" H 2631 1005 50  0000 R CNN
F 2 "Kosmo_panel:Potentiometer_Alpha_RD901F-40-00D_Single_Vertical_3D" H 2700 1050 50  0001 C CNN
F 3 "~" H 2700 1050 50  0001 C CNN
	1    2700 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2550 1350 3200 1350
Wire Wire Line
	3300 1650 3300 1150
Wire Wire Line
	3300 1150 3400 1150
Wire Wire Line
	2250 1650 3300 1650
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J12
U 1 1 60A972E3
P 1800 2300
F 0 "J12" H 1850 2617 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 1850 2526 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x03_P2.54mm_Vertical" H 1800 2300 50  0001 C CNN
F 3 "~" H 1800 2300 50  0001 C CNN
	1    1800 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 950  3400 950 
Wire Wire Line
	3200 950  3200 1350
Wire Wire Line
	2850 1050 3400 1050
Wire Wire Line
	1300 2200 1600 2200
Wire Wire Line
	2100 2200 2400 2200
Wire Wire Line
	1150 2400 1600 2400
Wire Wire Line
	4250 4550 4700 4550
Wire Wire Line
	4700 4550 4700 4250
Wire Wire Line
	4700 4250 5000 4250
Wire Wire Line
	4250 4650 4850 4650
Wire Wire Line
	4850 4650 4850 4350
Wire Wire Line
	4850 4350 5000 4350
$EndSCHEMATC
