EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 12
Title "Teen Kosmology"
Date ""
Rev "0.1"
Comp "HB"
Comment1 "UNTESTED UNTESTED UNTESTED UNTESTED"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L teensy:Adafruit_1.8TFT U?
U 1 1 6065855E
P 4400 3550
AR Path="/6065855E" Ref="U?"  Part="1" 
AR Path="/60656E29/6065855E" Ref="U5"  Part="1" 
F 0 "U5" H 4433 3575 50  0000 C CNN
F 1 "Adafruit_1.8TFT" H 4433 3484 50  0000 C CNN
F 2 "" H 4400 3550 50  0001 C CNN
F 3 "" H 4400 3550 50  0001 C CNN
	1    4400 3550
	1    0    0    -1  
$EndComp
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 603DC251
P 7500 4300
F 0 "A1" H 7500 3211 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 7500 3120 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 7500 4300 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 7500 4300 50  0001 C CNN
	1    7500 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 603E97A5
P 8350 3450
AR Path="/60656E29/603DB7F3/603E97A5" Ref="R?"  Part="1" 
AR Path="/60656E29/603E97A5" Ref="R3"  Part="1" 
F 0 "R3" H 8420 3496 50  0000 L CNN
F 1 "4.7k" H 8420 3405 50  0000 L CNN
F 2 "" V 8280 3450 50  0001 C CNN
F 3 "~" H 8350 3450 50  0001 C CNN
	1    8350 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 603E97AB
P 8700 3450
AR Path="/60656E29/603DB7F3/603E97AB" Ref="R?"  Part="1" 
AR Path="/60656E29/603E97AB" Ref="R4"  Part="1" 
F 0 "R4" H 8770 3496 50  0000 L CNN
F 1 "4.7k" H 8770 3405 50  0000 L CNN
F 2 "" V 8630 3450 50  0001 C CNN
F 3 "~" H 8700 3450 50  0001 C CNN
	1    8700 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 4700 8350 4700
Wire Wire Line
	8000 4800 8700 4800
Wire Wire Line
	7400 2950 7400 3150
NoConn ~ 7600 3300
Wire Wire Line
	7700 3300 7900 3300
Wire Wire Line
	8350 3600 8350 4700
Connection ~ 8350 4700
Wire Wire Line
	8350 4700 8800 4700
Wire Wire Line
	8350 3300 8700 3300
Connection ~ 8350 3300
Wire Wire Line
	8700 3600 8700 4800
Connection ~ 8700 4800
Wire Wire Line
	8700 4800 8800 4800
Text GLabel 6800 5000 0    50   Input ~ 0
TFT_SCK
Wire Wire Line
	7600 5300 7600 5400
Wire Wire Line
	7500 5300 7500 5400
Wire Wire Line
	7500 5400 7600 5400
Connection ~ 7600 5400
Text HLabel 8800 4800 2    50   Input ~ 0
SCL
Text HLabel 8800 4700 2    50   Input ~ 0
SCA
Text HLabel 7400 2950 1    50   Input ~ 0
5V
Text HLabel 7600 5700 3    50   Input ~ 0
GND
Wire Wire Line
	7600 5400 7600 5600
Connection ~ 7400 3150
Wire Wire Line
	7400 3150 7400 3300
Connection ~ 7600 5600
Wire Wire Line
	7600 5600 7600 5700
Wire Wire Line
	4700 4550 5650 4550
Text GLabel 6800 4900 0    50   Input ~ 0
TFT_MISO
Text GLabel 6800 4800 0    50   Input ~ 0
TFT_MOSI
Text GLabel 6800 4700 0    50   Input ~ 0
TFT_CS
Text GLabel 6800 4500 0    50   Input ~ 0
TFT_D_C
Text GLabel 6800 4400 0    50   Input ~ 0
TFT_RST
Wire Wire Line
	6800 5000 7000 5000
Wire Wire Line
	7000 4900 6800 4900
Wire Wire Line
	6800 4800 7000 4800
Wire Wire Line
	7000 4700 6800 4700
Wire Wire Line
	6800 4500 7000 4500
Wire Wire Line
	7000 4400 6800 4400
Text GLabel 4900 3950 2    50   Input ~ 0
TFT_SCK
Text GLabel 4900 3850 2    50   Input ~ 0
TFT_MISO
Text GLabel 4900 4050 2    50   Input ~ 0
TFT_MOSI
Text GLabel 4900 4150 2    50   Input ~ 0
TFT_CS
Text GLabel 4900 4350 2    50   Input ~ 0
TFT_D_C
Text GLabel 4900 4450 2    50   Input ~ 0
TFT_RST
Wire Wire Line
	4900 3850 4700 3850
Wire Wire Line
	4700 3950 4900 3950
Wire Wire Line
	4900 4050 4700 4050
Wire Wire Line
	4700 4150 4900 4150
Wire Wire Line
	4900 4350 4700 4350
Wire Wire Line
	4700 4450 4900 4450
Wire Wire Line
	4700 4650 5600 4650
Wire Wire Line
	5600 4650 5600 5600
$Comp
L Device:Rotary_Encoder_Switch SW?
U 1 1 60537D3C
P 3600 2700
AR Path="/60537D3C" Ref="SW?"  Part="1" 
AR Path="/60656E29/60537D3C" Ref="SW1"  Part="1" 
F 0 "SW1" H 3600 3067 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 3600 2976 50  0000 C CNN
F 2 "" H 3450 2860 50  0001 C CNN
F 3 "~" H 3600 2960 50  0001 C CNN
	1    3600 2700
	1    0    0    -1  
$EndComp
Text GLabel 3100 2600 0    50   Input ~ 0
Rotary_A
Text GLabel 3100 2800 0    50   Input ~ 0
Rotary_B
Text GLabel 3100 2700 0    50   Input ~ 0
Rotarý_C
Wire Wire Line
	3100 2600 3300 2600
Wire Wire Line
	3100 2700 3300 2700
Wire Wire Line
	3100 2800 3300 2800
Text GLabel 4050 2800 2    50   Input ~ 0
Rotary_S
Wire Wire Line
	3900 2800 4050 2800
Wire Notes Line
	4600 1900 4600 3250
Text Notes 4300 2100 2    50   ~ 0
Rotary Encoder with push button for menus
Wire Notes Line
	4600 3250 2500 3250
Wire Notes Line
	2500 3250 2500 1900
Wire Notes Line
	2500 1900 4600 1900
Text GLabel 6850 3900 0    50   Input ~ 0
Rotary_A
Text GLabel 6850 4100 0    50   Input ~ 0
Rotary_B
Text GLabel 6850 4000 0    50   Input ~ 0
Rotarý_C
Text GLabel 6850 4200 0    50   Input ~ 0
Rotary_S
Wire Wire Line
	6850 3900 7000 3900
Wire Wire Line
	6850 4000 7000 4000
Wire Wire Line
	6850 4100 7000 4100
Wire Wire Line
	6850 4200 7000 4200
Connection ~ 7900 3300
Wire Wire Line
	7900 3300 8350 3300
Wire Wire Line
	5650 3150 7400 3150
Wire Wire Line
	5600 5600 7600 5600
Wire Wire Line
	7900 2600 7900 3300
Wire Wire Line
	3900 2600 7900 2600
Wire Notes Line
	3900 3400 5900 3400
Wire Notes Line
	5900 3400 5900 5000
Wire Notes Line
	5900 5000 3900 5000
Wire Notes Line
	3900 5000 3900 3400
Text Notes 4200 4850 0    50   ~ 0
TFT Display on front panel
Text Notes 7850 1350 2    50   ~ 0
This is the digital UI and comprises of a display with SD card reader and a rotary encoder with push button for easy menu navigation
Text GLabel 4900 4250 2    50   Input ~ 0
TFT_CARD_CS
Text GLabel 6800 4600 0    50   Input ~ 0
TFT_CARD_CS
Wire Wire Line
	6800 4600 7000 4600
Wire Wire Line
	4700 4250 4900 4250
Wire Wire Line
	5650 3150 5650 4550
Text GLabel 4900 3750 2    50   Input ~ 0
TFT_LITE
Text GLabel 6800 4300 0    50   Input ~ 0
TFT_LITE
Wire Wire Line
	6800 4300 7000 4300
Wire Wire Line
	4700 3750 4900 3750
$EndSCHEMATC
