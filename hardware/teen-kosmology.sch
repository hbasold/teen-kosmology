EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 12
Title "Teen Kosmology"
Date ""
Rev "0.2"
Comp "HB"
Comment1 "UNTESTED UNTESTED UNTESTED UNTESTED"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR08
U 1 1 600BA0B0
P 5250 5850
F 0 "#PWR08" H 5250 5700 50  0001 C CNN
F 1 "+5V" H 5265 6023 50  0000 C CNN
F 2 "" H 5250 5850 50  0001 C CNN
F 3 "" H 5250 5850 50  0001 C CNN
	1    5250 5850
	1    0    0    -1  
$EndComp
$Sheet
S 8000 800  550  500 
U 606FF3B6
F0 "Input Potentiometers" 50
F1 "input-pots.sch" 50
F2 "A" I L 8000 900 50 
F3 "S0" I L 8000 1000 50 
F4 "S1" I L 8000 1100 50 
F5 "S2" I L 8000 1200 50 
$EndSheet
$Sheet
S 3300 2350 750  850 
U 6015C780
F0 "CV Inputs" 50
F1 "cv-inputs.sch" 50
F2 "CLK" I R 4050 2450 50 
F3 "DOut" I R 4050 2550 50 
F4 "DIn" I R 4050 2650 50 
F5 "NegCS" I R 4050 2750 50 
F6 "CV1" O L 3300 2400 50 
F7 "CV2" O L 3300 2500 50 
F8 "CV3" O L 3300 2600 50 
F9 "CV4" O L 3300 2700 50 
F10 "CV5" O L 3300 2800 50 
F11 "CV6" O L 3300 2900 50 
F12 "CV7" O L 3300 3000 50 
F13 "CV8" O L 3300 3100 50 
$EndSheet
Text GLabel 4250 2450 2    50   Input ~ 0
ADC_CLK
Text GLabel 4250 2750 2    50   Input ~ 0
ADC_CS
Text GLabel 4250 2550 2    50   Input ~ 0
ADC_DOut
Text GLabel 4250 2650 2    50   Input ~ 0
ADC_DIn
Wire Wire Line
	4050 2450 4250 2450
Wire Wire Line
	4050 2550 4250 2550
Wire Wire Line
	4050 2650 4250 2650
Wire Wire Line
	4050 2750 4250 2750
Wire Notes Line
	3550 7800 3550 4750
Text Notes 550  6050 0    50   ~ 0
Power input with reverse voltage protection\nand fuses for short-circuit protection\n(could be left out, if the user feels comfortable)
Text Notes 7800 4950 2    50   ~ 0
MIDI Input\n
$Sheet
S 1400 2750 850  250 
U 604EEE41
F0 "Line Out Gain" 50
F1 "lineout-gain.sch" 50
F2 "Line_Out_L" I L 1400 2800 50 
F3 "Line_Out_R" I L 1400 2900 50 
F4 "Out_R" O R 2250 2900 50 
F5 "Out_L" O R 2250 2800 50 
$EndSheet
Wire Wire Line
	10250 4000 10400 4000
Wire Notes Line
	9400 3450 11150 3450
Wire Notes Line
	9400 3450 9400 4750
Text Notes 9600 3600 0    50   ~ 0
OLED display
Text Notes 2900 2200 0    50   ~ 0
8 CV inputs converted to digital signals
Wire Wire Line
	10250 3800 10250 3900
$Comp
L power:+5V #PWR?
U 1 1 6049CC6C
P 10250 3800
AR Path="/60656E29/6049CC6C" Ref="#PWR?"  Part="1" 
AR Path="/6049CC6C" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 10250 3650 50  0001 C CNN
F 1 "+5V" H 10265 3973 50  0000 C CNN
F 2 "" H 10250 3800 50  0001 C CNN
F 3 "" H 10250 3800 50  0001 C CNN
	1    10250 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 3900 10400 3900
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 6049CC5E
P 10600 4000
AR Path="/60656E29/6049CC5E" Ref="J?"  Part="1" 
AR Path="/6049CC5E" Ref="J1"  Part="1" 
F 0 "J1" H 10680 3992 50  0000 L CNN
F 1 "Conn_01x04" H 10680 3901 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 10600 4000 50  0001 C CNN
F 3 "~" H 10600 4000 50  0001 C CNN
	1    10600 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6049CC64
P 10250 4400
AR Path="/60656E29/6049CC64" Ref="#PWR?"  Part="1" 
AR Path="/6049CC64" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 10250 4150 50  0001 C CNN
F 1 "GND" H 10255 4227 50  0000 C CNN
F 2 "" H 10250 4400 50  0001 C CNN
F 3 "" H 10250 4400 50  0001 C CNN
	1    10250 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 4400 10250 4000
Text GLabel 10100 4100 0    50   Input ~ 0
OLED_SDA
Text GLabel 10100 4200 0    50   Input ~ 0
OLED_SCL
Wire Wire Line
	10400 4200 10100 4200
Wire Wire Line
	10100 4100 10400 4100
Wire Notes Line
	3950 500  3950 1650
Wire Notes Line
	500  1650 3950 1650
$Comp
L Device:R R2
U 1 1 6011CE1B
P 8800 5550
F 0 "R2" V 8593 5550 50  0000 C CNN
F 1 "220" V 8684 5550 50  0000 C CNN
F 2 "" V 8730 5550 50  0001 C CNN
F 3 "~" H 8800 5550 50  0001 C CNN
	1    8800 5550
	0    1    1    0   
$EndComp
$Comp
L Diode:1N4148 D1
U 1 1 6012B917
P 9050 5700
F 0 "D1" V 9004 5780 50  0000 L CNN
F 1 "1N4148" V 9095 5780 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 9050 5525 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 9050 5700 50  0001 C CNN
	1    9050 5700
	0    1    1    0   
$EndComp
$Comp
L Isolator:6N137 U2
U 1 1 60142D8F
P 9700 5550
F 0 "U2" H 9700 6017 50  0000 C CNN
F 1 "6N137" H 9700 5926 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 9700 5050 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-0940EN" H 8850 6100 50  0001 C CNN
	1    9700 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 5550 9400 5550
Wire Wire Line
	9050 5850 9400 5850
Wire Wire Line
	9400 5850 9400 5750
NoConn ~ 10000 5450
$Comp
L power:+3.3V #PWR05
U 1 1 6016FE94
P 10100 5050
F 0 "#PWR05" H 10100 4900 50  0001 C CNN
F 1 "+3.3V" H 10115 5223 50  0000 C CNN
F 2 "" H 10100 5050 50  0001 C CNN
F 3 "" H 10100 5050 50  0001 C CNN
	1    10100 5050
	1    0    0    -1  
$EndComp
Text GLabel 10850 5550 2    50   Input ~ 0
MidiIn
$Comp
L power:GND #PWR010
U 1 1 6015E9F8
P 10550 5750
F 0 "#PWR010" H 10550 5500 50  0001 C CNN
F 1 "GND" V 10555 5622 50  0000 R CNN
F 2 "" H 10550 5750 50  0001 C CNN
F 3 "" H 10550 5750 50  0001 C CNN
	1    10550 5750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10000 5350 10100 5350
Connection ~ 10100 5350
Wire Wire Line
	10100 5350 10250 5350
Wire Wire Line
	8950 5550 9050 5550
Connection ~ 9050 5550
Wire Wire Line
	8250 5550 8650 5550
Wire Wire Line
	8050 5550 8050 5850
Wire Wire Line
	8050 5850 9050 5850
Connection ~ 9050 5850
$Comp
L Device:C C2
U 1 1 6046FABE
P 8150 6100
F 0 "C2" H 8265 6146 50  0000 L CNN
F 1 "0.1uF" H 8265 6055 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 8188 5950 50  0001 C CNN
F 3 "~" H 8150 6100 50  0001 C CNN
	1    8150 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 60472D1B
P 8150 6250
F 0 "#PWR012" H 8150 6000 50  0001 C CNN
F 1 "GND" H 8155 6077 50  0000 C CNN
F 2 "" H 8150 6250 50  0001 C CNN
F 3 "" H 8150 6250 50  0001 C CNN
	1    8150 6250
	1    0    0    -1  
$EndComp
Wire Notes Line
	7000 4750 7000 6500
Wire Wire Line
	10000 5550 10250 5550
Connection ~ 10250 5550
Wire Wire Line
	10250 5550 10850 5550
$Comp
L Device:R_Small R1
U 1 1 601699EB
P 10250 5450
F 0 "R1" H 10309 5496 50  0000 L CNN
F 1 "10k" H 10309 5405 50  0000 L CNN
F 2 "" H 10250 5450 50  0001 C CNN
F 3 "~" H 10250 5450 50  0001 C CNN
	1    10250 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 5250 10300 5250
Wire Wire Line
	10500 5750 10000 5750
Wire Wire Line
	10550 5750 10500 5750
Connection ~ 10500 5750
Wire Wire Line
	10500 5750 10500 5250
Wire Wire Line
	10100 5050 10100 5250
Wire Wire Line
	10100 5250 10100 5350
Connection ~ 10100 5250
$Comp
L Device:C_Small C1
U 1 1 601766A0
P 10200 5250
F 0 "C1" V 9971 5250 50  0000 C CNN
F 1 "0.1uF" V 10062 5250 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 10200 5250 50  0001 C CNN
F 3 "~" H 10200 5250 50  0001 C CNN
	1    10200 5250
	0    1    1    0   
$EndComp
$Comp
L Device:Rotary_Encoder_Switch SW1
U 1 1 609ED349
P 1500 1050
AR Path="/609ED349" Ref="SW1"  Part="1" 
AR Path="/60656E29/609ED349" Ref="SW?"  Part="1" 
F 0 "SW1" H 1500 1417 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 1500 1326 50  0000 C CNN
F 2 "" H 1350 1210 50  0001 C CNN
F 3 "~" H 1500 1310 50  0001 C CNN
	1    1500 1050
	1    0    0    -1  
$EndComp
Text GLabel 1200 1150 0    50   Input ~ 0
Rotary1_B
Text GLabel 1800 950  2    50   Input ~ 0
Rotary1_S
Text Notes 2450 600  2    50   ~ 0
Rotary encoders with push button for menus
$Comp
L power:GND #PWR01
U 1 1 609ED355
P 1800 1400
F 0 "#PWR01" H 1800 1150 50  0001 C CNN
F 1 "GND" H 1805 1227 50  0000 C CNN
F 2 "" H 1800 1400 50  0001 C CNN
F 3 "" H 1800 1400 50  0001 C CNN
	1    1800 1400
	1    0    0    -1  
$EndComp
Text GLabel 1200 950  0    50   Input ~ 0
Rotary1_A
Wire Wire Line
	1200 1050 700  1050
Wire Wire Line
	700  1050 700  1400
Wire Wire Line
	700  1400 1800 1400
Wire Wire Line
	1800 1150 1800 1400
Connection ~ 1800 1400
$Comp
L Device:Rotary_Encoder_Switch SW2
U 1 1 60C42ECE
P 3150 1050
AR Path="/60C42ECE" Ref="SW2"  Part="1" 
AR Path="/60656E29/60C42ECE" Ref="SW?"  Part="1" 
F 0 "SW2" H 3150 1417 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 3150 1326 50  0000 C CNN
F 2 "" H 3000 1210 50  0001 C CNN
F 3 "~" H 3150 1310 50  0001 C CNN
	1    3150 1050
	1    0    0    -1  
$EndComp
Text GLabel 2850 1150 0    50   Input ~ 0
Rotary2_B
Text GLabel 3450 950  2    50   Input ~ 0
Rotary2_S
$Comp
L power:GND #PWR02
U 1 1 60C42ED6
P 3450 1400
F 0 "#PWR02" H 3450 1150 50  0001 C CNN
F 1 "GND" H 3455 1227 50  0000 C CNN
F 2 "" H 3450 1400 50  0001 C CNN
F 3 "" H 3450 1400 50  0001 C CNN
	1    3450 1400
	1    0    0    -1  
$EndComp
Text GLabel 2850 950  0    50   Input ~ 0
Rotary2_A
Wire Wire Line
	2850 1050 2350 1050
Wire Wire Line
	2350 1050 2350 1400
Wire Wire Line
	2350 1400 3450 1400
Wire Wire Line
	3450 1150 3450 1400
Connection ~ 3450 1400
Wire Wire Line
	2250 2800 2300 2800
Wire Wire Line
	2250 2900 2300 2900
Text GLabel 1300 2800 0    50   Input ~ 0
Line_Out_L
Wire Wire Line
	1300 2800 1400 2800
Text GLabel 1300 2900 0    50   Input ~ 0
Line_Out_R
Wire Wire Line
	1300 2900 1400 2900
$Sheet
S 5650 800  950  750 
U 60DD7607
F0 "Audio Board Conns" 50
F1 "AudioBoard.sch" 50
$EndSheet
$Sheet
S 4400 800  1000 750 
U 60E332D6
F0 "Teensy Conns" 50
F1 "Teensy.sch" 50
$EndSheet
Text GLabel 8000 1000 0    50   Input ~ 0
Pot_S0
Text GLabel 8000 1100 0    50   Input ~ 0
Pot_S1
Text GLabel 8000 1200 0    50   Input ~ 0
Pot_S2
Text GLabel 8000 900  0    50   Input ~ 0
Pot_A
Text GLabel 1250 2500 0    50   Output ~ 0
Line_In_R
Text GLabel 1250 2400 0    50   Output ~ 0
Line_In_L
$Sheet
S 1400 2350 800  250 
U 60190F78
F0 "Line In" 50
F1 "linein.sch" 50
F2 "Line_In_L" I L 1400 2400 50 
F3 "Line_In_R" I L 1400 2500 50 
F4 "In_L" I R 2200 2400 50 
F5 "In_R" I R 2200 2500 50 
$EndSheet
Wire Wire Line
	2200 2500 2300 2500
Wire Wire Line
	1250 2400 1400 2400
Wire Wire Line
	1250 2500 1400 2500
Wire Wire Line
	3100 2400 3300 2400
Wire Wire Line
	3300 2500 3100 2500
Wire Wire Line
	3300 2600 3100 2600
Wire Wire Line
	3100 2700 3300 2700
Wire Wire Line
	3300 2800 3100 2800
Wire Wire Line
	3100 2900 3300 2900
Wire Wire Line
	3100 3000 3300 3000
Wire Wire Line
	3100 3100 3300 3100
$Sheet
S 1100 5100 550  300 
U 6113F21E
F0 "sheet6113F21A" 50
F1 "voltage-ref.sch" 50
F2 "AREF" I L 1100 5150 50 
F3 "AREF_MID" I L 1100 5350 50 
$EndSheet
Wire Wire Line
	800  5350 1100 5350
Wire Wire Line
	950  5150 1100 5150
$Comp
L power:+5VA #PWR06
U 1 1 61152CB2
P 950 5150
F 0 "#PWR06" H 950 5000 50  0001 C CNN
F 1 "+5VA" H 965 5323 50  0000 C CNN
F 2 "" H 950 5150 50  0001 C CNN
F 3 "" H 950 5150 50  0001 C CNN
	1    950  5150
	1    0    0    -1  
$EndComp
$Comp
L power:+2V5 #PWR07
U 1 1 61159322
P 800 5350
F 0 "#PWR07" H 800 5200 50  0001 C CNN
F 1 "+2V5" H 815 5523 50  0000 C CNN
F 2 "" H 800 5350 50  0001 C CNN
F 3 "" H 800 5350 50  0001 C CNN
	1    800  5350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J5
U 1 1 6078E8C4
P 1100 6950
F 0 "J5" H 1150 7367 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 1150 7276 50  0000 C CNN
F 2 "Kosmo_panel:Power_Header" H 1100 6950 50  0001 C CNN
F 3 "~" H 1100 6950 50  0001 C CNN
	1    1100 6950
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR013
U 1 1 6079637D
P 3350 6350
F 0 "#PWR013" H 3350 6450 50  0001 C CNN
F 1 "-12V" H 3365 6523 50  0000 C CNN
F 2 "" H 3350 6350 50  0001 C CNN
F 3 "" H 3350 6350 50  0001 C CNN
	1    3350 6350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 60796383
P 1550 6950
F 0 "#PWR015" H 1550 6700 50  0001 C CNN
F 1 "GND" H 1555 6777 50  0000 C CNN
F 2 "" H 1550 6950 50  0001 C CNN
F 3 "" H 1550 6950 50  0001 C CNN
	1    1550 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 6950 1550 6950
$Comp
L power:+12V #PWR018
U 1 1 6079638E
P 3200 7550
F 0 "#PWR018" H 3200 7400 50  0001 C CNN
F 1 "+12V" H 3215 7723 50  0000 C CNN
F 2 "" H 3200 7550 50  0001 C CNN
F 3 "" H 3200 7550 50  0001 C CNN
	1    3200 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 6850 1400 6950
Connection ~ 1400 6950
Wire Wire Line
	1400 6950 1400 7050
$Comp
L power:GND #PWR014
U 1 1 607AD6AB
P 700 6950
F 0 "#PWR014" H 700 6700 50  0001 C CNN
F 1 "GND" H 705 6777 50  0000 C CNN
F 2 "" H 700 6950 50  0001 C CNN
F 3 "" H 700 6950 50  0001 C CNN
	1    700  6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  6950 900  6950
Wire Wire Line
	900  6850 900  6950
Connection ~ 900  6950
Wire Wire Line
	900  6950 900  7050
Wire Wire Line
	900  7150 900  7550
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 60862D8A
P 6200 5850
F 0 "J3" H 6280 5842 50  0000 L CNN
F 1 "Conn_01x02" H 6280 5751 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 6200 5850 50  0001 C CNN
F 3 "~" H 6200 5850 50  0001 C CNN
	1    6200 5850
	1    0    0    -1  
$EndComp
Wire Notes Line
	7000 4750 11200 4750
Text Notes 3750 5200 0    50   ~ 0
5V Step-Down Buck Converter (the pins can be used to tap into the 5V\nsupply or to use an external supply, if the buck converter is not installed)\ndesigned for 500mA.
Wire Notes Line
	3550 5700 500  5700
Wire Notes Line
	450  4750 6950 4750
Text Notes 600  4850 0    50   ~ 0
Reference voltage for ADC
$Comp
L Device:Polyfuse F2
U 1 1 60985A2F
P 2000 7550
F 0 "F2" V 1900 7550 50  0000 C CNN
F 1 "0.5A Polyfuse (RXEF050)" V 2100 7550 50  0000 C CNN
F 2 "Fuse:Fuse_BelFuse_0ZRE0005FF_L8.3mm_W3.8mm" H 2050 7350 50  0001 L CNN
F 3 "~" H 2000 7550 50  0001 C CNN
	1    2000 7550
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 607CB4DE
P 2650 7100
F 0 "R6" H 2720 7146 50  0000 L CNN
F 1 "100" H 2720 7055 50  0000 L CNN
F 2 "" V 2580 7100 50  0001 C CNN
F 3 "~" H 2650 7100 50  0001 C CNN
	1    2650 7100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF4905 Q2
U 1 1 607895CA
P 2650 7450
F 0 "Q2" V 2650 7700 50  0000 C CNN
F 1 "IRF4905" V 2550 7700 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2850 7375 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/irf4905.pdf?fileId=5546d462533600a4015355e32165197c" H 2650 7450 50  0001 L CNN
	1    2650 7450
	0    -1   1    0   
$EndComp
Connection ~ 1550 6950
Wire Wire Line
	1400 7550 1850 7550
$Comp
L Transistor_FET:IPP060N06N Q1
U 1 1 6082C729
P 2650 6450
F 0 "Q1" V 2992 6450 50  0000 C CNN
F 1 "STP55NF06" V 2901 6450 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2900 6375 50  0001 L CIN
F 3 "https://www.infineon.com/dgdl/Infineon-IPP060N06N-DS-v02_02-en.pdf?fileId=db3a30433727a44301372c06d9d7498a" H 2650 6450 50  0001 L CNN
	1    2650 6450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 60839A60
P 2650 6800
F 0 "R5" H 2720 6846 50  0000 L CNN
F 1 "100" H 2720 6755 50  0000 L CNN
F 2 "" V 2580 6800 50  0001 C CNN
F 3 "~" H 2650 6800 50  0001 C CNN
	1    2650 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 6650 3000 6650
Connection ~ 2650 6650
$Comp
L Device:Polyfuse F1
U 1 1 60844713
P 2050 6350
F 0 "F1" V 1850 6350 50  0000 C CNN
F 1 "0.5A Polyfuse (RXEF050)" V 1950 6250 50  0000 C CNN
F 2 "Fuse:Fuse_BelFuse_0ZRE0005FF_L8.3mm_W3.8mm" H 2100 6150 50  0001 L CNN
F 3 "~" H 2050 6350 50  0001 C CNN
	1    2050 6350
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 7550 2450 7550
Wire Wire Line
	3000 7250 2650 7250
Connection ~ 2650 7250
Wire Wire Line
	2650 6950 1550 6950
Wire Wire Line
	2200 6350 2450 6350
Connection ~ 2650 6950
Wire Wire Line
	1900 6350 1400 6350
Wire Wire Line
	900  6750 900  6350
Wire Wire Line
	900  6350 1400 6350
Wire Wire Line
	1400 7150 1400 7550
Wire Wire Line
	900  7550 1400 7550
Connection ~ 1400 6350
Wire Wire Line
	1400 6350 1400 6750
Connection ~ 1400 7550
Wire Wire Line
	2850 7550 3000 7550
$Comp
L Diode:1N47xxA D3
U 1 1 608D908C
P 3000 7400
F 0 "D3" V 3350 7450 50  0000 R CNN
F 1 "1N4743A" V 3250 7500 50  0000 R CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3000 7225 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 3000 7400 50  0001 C CNN
	1    3000 7400
	0    -1   -1   0   
$EndComp
Connection ~ 3000 7550
Wire Wire Line
	3000 7550 3200 7550
$Comp
L Diode:1N47xxA D2
U 1 1 607A0AD0
P 3000 6500
F 0 "D2" V 3350 6550 50  0000 R CNN
F 1 "1N4743A" V 3250 6600 50  0000 R CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3000 6325 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 3000 6500 50  0001 C CNN
	1    3000 6500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2850 6350 3000 6350
Connection ~ 3000 6350
Wire Wire Line
	3000 6350 3350 6350
$Comp
L power:GND #PWR?
U 1 1 608600C6
P 2950 3900
AR Path="/60B2848E/608600C6" Ref="#PWR?"  Part="1" 
AR Path="/608600C6" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 2950 3650 50  0001 C CNN
F 1 "GND" H 2955 3727 50  0000 C CNN
F 2 "" H 2950 3900 50  0001 C CNN
F 3 "" H 2950 3900 50  0001 C CNN
	1    2950 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 608600CC
P 1750 3900
AR Path="/60B2848E/608600CC" Ref="#PWR?"  Part="1" 
AR Path="/608600CC" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 1750 3650 50  0001 C CNN
F 1 "GND" H 1755 3727 50  0000 C CNN
F 2 "" H 1750 3900 50  0001 C CNN
F 3 "" H 1750 3900 50  0001 C CNN
	1    1750 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 3600 2100 3600
Wire Wire Line
	1750 3900 1750 3600
Connection ~ 1750 3900
Wire Wire Line
	1750 3900 2100 3900
Wire Wire Line
	2950 3600 2600 3600
Wire Wire Line
	2950 3900 2950 3600
Connection ~ 2950 3900
Wire Wire Line
	2600 3900 2950 3900
Text GLabel 2100 4000 0    50   Input ~ 0
CV1
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J?
U 1 1 608600BA
P 2300 3900
AR Path="/60B2848E/608600BA" Ref="J?"  Part="1" 
AR Path="/608600BA" Ref="J11"  Part="1" 
F 0 "J11" H 2350 4417 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 2350 4326 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x10_P2.54mm_Vertical" H 2300 3900 50  0001 C CNN
F 3 "~" H 2300 3900 50  0001 C CNN
	1    2300 3900
	1    0    0    -1  
$EndComp
Text GLabel 2100 4100 0    50   Input ~ 0
CV3
Text GLabel 2100 4200 0    50   Input ~ 0
CV5
Text GLabel 2100 4300 0    50   Input ~ 0
CV7
Text GLabel 2600 4300 2    50   Input ~ 0
CV2
Text GLabel 2600 4200 2    50   Input ~ 0
CV4
Text GLabel 2600 4100 2    50   Input ~ 0
CV6
Text GLabel 2600 4000 2    50   Input ~ 0
CV8
Text GLabel 2100 3700 0    50   Input ~ 0
In_R
Text GLabel 2600 3800 2    50   Output ~ 0
Out_R
Text GLabel 2100 3800 0    50   Input ~ 0
In_L
Text GLabel 2600 3700 2    50   Output ~ 0
Out_L
Wire Wire Line
	2200 2400 2300 2400
Text GLabel 2300 2400 2    50   Input ~ 0
In_L
Text GLabel 2300 2500 2    50   Input ~ 0
In_R
Text GLabel 2300 2800 2    50   Output ~ 0
Out_L
Text GLabel 2300 2900 2    50   Output ~ 0
Out_R
Text GLabel 3100 2400 0    50   Input ~ 0
CV1
Text GLabel 3100 2600 0    50   Input ~ 0
CV3
Text GLabel 3100 2800 0    50   Input ~ 0
CV5
Text GLabel 3100 3000 0    50   Input ~ 0
CV7
Text GLabel 3100 2500 0    50   Input ~ 0
CV2
Text GLabel 3100 2700 0    50   Input ~ 0
CV4
Text GLabel 3100 2900 0    50   Input ~ 0
CV6
Text GLabel 3100 3100 0    50   Input ~ 0
CV8
$Comp
L Regulator_Switching:AP63205WU U3
U 1 1 6086A2F6
P 4750 6900
F 0 "U3" H 4750 7267 50  0000 C CNN
F 1 "AP63205WU" H 4750 7176 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-6" H 4750 6000 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/AP63200-AP63201-AP63203-AP63205.pdf" H 4750 6900 50  0001 C CNN
	1    4750 6900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 6086BCC2
P 4750 7350
F 0 "#PWR0116" H 4750 7100 50  0001 C CNN
F 1 "GND" H 4755 7177 50  0000 C CNN
F 2 "" H 4750 7350 50  0001 C CNN
F 3 "" H 4750 7350 50  0001 C CNN
	1    4750 7350
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 6086C1FE
P 5600 6900
F 0 "L1" V 5790 6900 50  0000 C CNN
F 1 "6.8uH" V 5699 6900 50  0000 C CNN
F 2 "" H 5600 6900 50  0001 C CNN
F 3 "~" H 5600 6900 50  0001 C CNN
	1    5600 6900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C3
U 1 1 6086C813
P 5300 6800
F 0 "C3" V 5048 6800 50  0000 C CNN
F 1 "0.1uF" V 5139 6800 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 5338 6650 50  0001 C CNN
F 3 "~" H 5300 6800 50  0001 C CNN
F 4 "Ceramic" H 5300 6800 50  0001 C CNN "Characteristics"
	1    5300 6800
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 6800 5450 6900
Wire Wire Line
	5450 6900 5150 6900
Connection ~ 5450 6900
Wire Wire Line
	5150 7000 5750 7000
Wire Wire Line
	5750 7000 5750 6900
$Comp
L Device:C C5
U 1 1 6087EAE6
P 5900 7050
F 0 "C5" H 6015 7096 50  0000 L CNN
F 1 "22uF" H 6015 7005 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 5938 6900 50  0001 C CNN
F 3 "~" H 5900 7050 50  0001 C CNN
F 4 "> 25V, MLCC" H 5900 7050 50  0001 C CNN "Characteristics"
	1    5900 7050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 6087F24F
P 6250 7200
F 0 "C6" H 6365 7246 50  0000 L CNN
F 1 "22uF" H 6365 7155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 6288 7050 50  0001 C CNN
F 3 "~" H 6250 7200 50  0001 C CNN
F 4 "> 25V, MLCC" H 6250 7200 50  0001 C CNN "Characteristics"
	1    6250 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 6900 5900 6900
Connection ~ 5750 6900
Wire Wire Line
	5900 7200 4750 7200
Connection ~ 4750 7200
$Comp
L power:+5V #PWR0117
U 1 1 60895586
P 6400 6900
F 0 "#PWR0117" H 6400 6750 50  0001 C CNN
F 1 "+5V" H 6415 7073 50  0000 C CNN
F 2 "" H 6400 6900 50  0001 C CNN
F 3 "" H 6400 6900 50  0001 C CNN
	1    6400 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 6089CB1F
P 4100 7050
F 0 "C4" H 4215 7096 50  0000 L CNN
F 1 "10uF" H 4215 7005 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 4138 6900 50  0001 C CNN
F 3 "~" H 4100 7050 50  0001 C CNN
F 4 "> 25V, MLCC" H 4100 7050 50  0001 C CNN "Description"
	1    4100 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 7200 4750 7200
Wire Wire Line
	4100 6900 4100 6800
Wire Wire Line
	4100 6800 4350 6800
Wire Wire Line
	4350 7000 4350 6800
Connection ~ 4350 6800
$Comp
L power:+12V #PWR0118
U 1 1 608BC27E
P 3900 6800
F 0 "#PWR0118" H 3900 6650 50  0001 C CNN
F 1 "+12V" H 3915 6973 50  0000 C CNN
F 2 "" H 3900 6800 50  0001 C CNN
F 3 "" H 3900 6800 50  0001 C CNN
	1    3900 6800
	1    0    0    -1  
$EndComp
Connection ~ 4100 6800
Wire Wire Line
	4750 7350 6250 7350
Wire Wire Line
	6250 7050 6250 6900
Wire Wire Line
	6250 6900 5900 6900
Connection ~ 5900 6900
Connection ~ 6250 6900
Wire Wire Line
	4750 7200 4750 7350
Connection ~ 4750 7350
Wire Wire Line
	6250 6900 6400 6900
Wire Wire Line
	3900 6800 4100 6800
$Comp
L Transistor_FET:IRF4905 Q7
U 1 1 6089F19D
P 5550 5950
F 0 "Q7" V 5550 6200 50  0000 C CNN
F 1 "IRF4905" V 5450 6200 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5750 5875 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/irf4905.pdf?fileId=5546d462533600a4015355e32165197c" H 5550 5950 50  0001 L CNN
	1    5550 5950
	0    1    -1   0   
$EndComp
Wire Wire Line
	5750 5850 6000 5850
Wire Wire Line
	5250 5850 5350 5850
$Comp
L power:GND #PWR09
U 1 1 600C429F
P 5550 6150
F 0 "#PWR09" H 5550 5900 50  0001 C CNN
F 1 "GND" H 5555 5977 50  0000 C CNN
F 2 "" H 5550 6150 50  0001 C CNN
F 3 "" H 5550 6150 50  0001 C CNN
	1    5550 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 6150 6000 6150
Wire Wire Line
	6000 6150 6000 5950
Connection ~ 5550 6150
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 60894666
P 8150 5350
F 0 "J2" V 8400 5300 50  0000 L CNN
F 1 "Conn_01x03" V 8300 5100 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-03A_1x03_P2.54mm_Vertical" H 8150 5350 50  0001 C CNN
F 3 "~" H 8150 5350 50  0001 C CNN
	1    8150 5350
	0    1    -1   0   
$EndComp
Wire Wire Line
	8150 5550 8150 5950
$EndSCHEMATC
