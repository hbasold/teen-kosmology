EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 9
Title "Teen Kosmology"
Date ""
Rev "0.2"
Comp "HB"
Comment1 "UNTESTED UNTESTED UNTESTED UNTESTED"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6000 2800 2    50   Input ~ 0
AREF
Wire Wire Line
	5250 2900 5250 3050
Wire Wire Line
	5850 3050 5850 2800
$Comp
L Amplifier_Operational:TL072 U?
U 1 1 60778210
P 5550 2800
AR Path="/6015C780/60173375/60778210" Ref="U?"  Part="1" 
AR Path="/6113F21E/60778210" Ref="U10"  Part="1" 
F 0 "U10" H 5550 3167 50  0000 C CNN
F 1 "TL072" H 5550 3076 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 5550 2800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 5550 2800 50  0001 C CNN
	1    5550 2800
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U?
U 2 1 607798E5
P 5050 3250
AR Path="/6015C780/60173375/607798E5" Ref="U?"  Part="2" 
AR Path="/6113F21E/607798E5" Ref="U10"  Part="2" 
F 0 "U10" H 5050 3617 50  0000 C CNN
F 1 "TL072" H 5050 3526 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 5050 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 5050 3250 50  0001 C CNN
	2    5050 3250
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U?
U 3 1 6077A4EF
P 7200 3300
AR Path="/6015C780/60173375/6077A4EF" Ref="U?"  Part="3" 
AR Path="/6113F21E/6077A4EF" Ref="U10"  Part="3" 
F 0 "U10" H 7158 3346 50  0000 L CNN
F 1 "TL072" H 7158 3255 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 7200 3300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 7200 3300 50  0001 C CNN
	3    7200 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 60780C87
P 7100 3000
AR Path="/6015C780/60173375/60780C87" Ref="#PWR?"  Part="1" 
AR Path="/6113F21E/60780C87" Ref="#PWR088"  Part="1" 
F 0 "#PWR088" H 7100 2850 50  0001 C CNN
F 1 "+12V" H 7115 3173 50  0000 C CNN
F 2 "" H 7100 3000 50  0001 C CNN
F 3 "" H 7100 3000 50  0001 C CNN
	1    7100 3000
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 60782513
P 7100 3600
AR Path="/6015C780/60173375/60782513" Ref="#PWR?"  Part="1" 
AR Path="/6113F21E/60782513" Ref="#PWR091"  Part="1" 
F 0 "#PWR091" H 7100 3700 50  0001 C CNN
F 1 "-12V" H 7115 3773 50  0000 C CNN
F 2 "" H 7100 3600 50  0001 C CNN
F 3 "" H 7100 3600 50  0001 C CNN
	1    7100 3600
	-1   0    0    1   
$EndComp
Connection ~ 5850 2800
Wire Wire Line
	5250 3050 5850 3050
$Comp
L Device:R_POT_TRIM RV?
U 1 1 60792CC4
P 4350 3150
AR Path="/6015C780/60173375/60792CC4" Ref="RV?"  Part="1" 
AR Path="/6113F21E/60792CC4" Ref="RV21"  Part="1" 
F 0 "RV21" H 4280 3196 50  0000 R CNN
F 1 "100k" H 4280 3105 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 4350 3150 50  0001 C CNN
F 3 "~" H 4350 3150 50  0001 C CNN
	1    4350 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2700 4350 3000
Wire Wire Line
	4350 3300 3600 3300
Wire Wire Line
	4500 3150 4750 3150
Wire Wire Line
	4750 3350 4750 3500
Wire Wire Line
	4750 3500 5350 3500
Wire Wire Line
	5350 3500 5350 3250
Text HLabel 5500 3250 2    50   Input ~ 0
AREF_MID
Wire Wire Line
	5350 3250 5500 3250
Connection ~ 5350 3250
Wire Wire Line
	5850 2800 6000 2800
Text Notes 3850 3800 0    50   ~ 0
Set the trim pot for the AREF_MID to about half way. You want\nto read about 2.5V at the output of the stage towards the ADC.
$Comp
L Reference_Voltage:LM4040DBZ-5 U11
U 1 1 60960EFC
P 3600 2850
F 0 "U11" V 3646 2762 50  0000 R CNN
F 1 "LM4040DBZ-5" V 3555 2762 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3600 2650 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm4040-n.pdf" H 3600 2850 50  0001 C CIN
	1    3600 2850
	0    -1   -1   0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 60963E29
P 3600 2400
AR Path="/6015C780/60173375/60963E29" Ref="#PWR?"  Part="1" 
AR Path="/6113F21E/60963E29" Ref="#PWR087"  Part="1" 
F 0 "#PWR087" H 3600 2250 50  0001 C CNN
F 1 "+12V" H 3615 2573 50  0000 C CNN
F 2 "" H 3600 2400 50  0001 C CNN
F 3 "" H 3600 2400 50  0001 C CNN
	1    3600 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R67
U 1 1 60964C4E
P 3600 2550
F 0 "R67" H 3670 2596 50  0000 L CNN
F 1 "36k" H 3670 2505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3530 2550 50  0001 C CNN
F 3 "~" H 3600 2550 50  0001 C CNN
	1    3600 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR089
U 1 1 60968862
P 3600 3300
F 0 "#PWR089" H 3600 3050 50  0001 C CNN
F 1 "GND" H 3605 3127 50  0000 C CNN
F 2 "" H 3600 3300 50  0001 C CNN
F 3 "" H 3600 3300 50  0001 C CNN
	1    3600 3300
	1    0    0    -1  
$EndComp
Text Notes 1550 2000 0    50   ~ 0
The TL072 requires 10nA at about 100°C and the LM4040-5.0 needs at least 80uA\nunder any temperature. The voltage divider for 2.5 requires about 50uA. To be on \nthe safe side, we estimate 200uA. With a 12V supply and 5V reference, we need\nR = (12 - 5)/(200u) = 35k and 36k should do the job
Wire Wire Line
	4350 2700 5250 2700
Wire Wire Line
	3600 2700 4350 2700
Wire Wire Line
	3600 3300 3600 3000
Connection ~ 3600 3300
Connection ~ 4350 2700
Connection ~ 3600 2700
$Comp
L Device:C C45
U 1 1 6087DC5D
P 7650 3150
F 0 "C45" H 7765 3196 50  0000 L CNN
F 1 "0.1uF" H 7765 3105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 7688 3000 50  0001 C CNN
F 3 "~" H 7650 3150 50  0001 C CNN
	1    7650 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C46
U 1 1 6087FF8E
P 7650 3450
F 0 "C46" H 7765 3496 50  0000 L CNN
F 1 "0.1uF" H 7765 3405 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 7688 3300 50  0001 C CNN
F 3 "~" H 7650 3450 50  0001 C CNN
	1    7650 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3000 7650 3000
Connection ~ 7100 3000
Wire Wire Line
	7650 3600 7100 3600
Connection ~ 7100 3600
$Comp
L power:GND #PWR090
U 1 1 608832B4
P 8150 3300
F 0 "#PWR090" H 8150 3050 50  0001 C CNN
F 1 "GND" H 8155 3127 50  0000 C CNN
F 2 "" H 8150 3300 50  0001 C CNN
F 3 "" H 8150 3300 50  0001 C CNN
	1    8150 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 3300 8150 3300
Connection ~ 7650 3300
$EndSCHEMATC
