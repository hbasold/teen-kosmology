EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 9
Title "Teen Kosmology"
Date ""
Rev "0.2"
Comp "HB"
Comment1 "UNTESTED UNTESTED UNTESTED UNTESTED"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR083
U 1 1 604F1F90
P 8700 5450
F 0 "#PWR083" H 8700 5200 50  0001 C CNN
F 1 "GND" H 8705 5277 50  0000 C CNN
F 2 "" H 8700 5450 50  0001 C CNN
F 3 "" H 8700 5450 50  0001 C CNN
	1    8700 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R66
U 1 1 604F757E
P 8700 5100
F 0 "R66" H 8630 5054 50  0000 R CNN
F 1 "2.2k" H 8630 5145 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8630 5100 50  0001 C CNN
F 3 "~" H 8700 5100 50  0001 C CNN
	1    8700 5100
	-1   0    0    1   
$EndComp
Wire Wire Line
	8700 5250 8700 5450
$Comp
L power:GND #PWR076
U 1 1 60521FB2
P 8750 2850
F 0 "#PWR076" H 8750 2600 50  0001 C CNN
F 1 "GND" H 8755 2677 50  0000 C CNN
F 2 "" H 8750 2850 50  0001 C CNN
F 3 "" H 8750 2850 50  0001 C CNN
	1    8750 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R62
U 1 1 60521FCA
P 8750 2600
F 0 "R62" H 8680 2554 50  0000 R CNN
F 1 "2.2k" H 8680 2645 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8680 2600 50  0001 C CNN
F 3 "~" H 8750 2600 50  0001 C CNN
	1    8750 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	8750 2750 8750 2800
Text Notes 6400 1650 0    50   ~ 0
3.3Vpp, centred around\n1.6V (SGTL5000)
Text Notes 8600 1250 0    50   ~ 0
about 15Vpp at maximum, 1.45V peak at minimum
$Comp
L power:+12V #PWR084
U 1 1 601000C0
P 1550 6050
F 0 "#PWR084" H 1550 5900 50  0001 C CNN
F 1 "+12V" H 1565 6223 50  0000 C CNN
F 2 "" H 1550 6050 50  0001 C CNN
F 3 "" H 1550 6050 50  0001 C CNN
	1    1550 6050
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR086
U 1 1 60102B10
P 1550 6650
F 0 "#PWR086" H 1550 6750 50  0001 C CNN
F 1 "-12V" H 1565 6823 50  0000 C CNN
F 2 "" H 1550 6650 50  0001 C CNN
F 3 "" H 1550 6650 50  0001 C CNN
	1    1550 6650
	-1   0    0    1   
$EndComp
Text HLabel 6850 4300 0    50   Input ~ 0
Line_Out_L
Text HLabel 6850 1800 0    50   Input ~ 0
Line_Out_R
Text HLabel 10400 1900 2    50   Output ~ 0
Out_R
Text HLabel 10400 4400 2    50   Output ~ 0
Out_L
$Comp
L Device:R_POT RV17
U 1 1 60A6727F
P 9550 2300
F 0 "RV17" H 9480 2346 50  0000 R CNN
F 1 "10k" H 9480 2255 50  0000 R CNN
F 2 "Kosmo_panel:Potentiometer_Alpha_RD901F-40-00D_Single_Vertical_3D" H 9550 2300 50  0001 C CNN
F 3 "~" H 9550 2300 50  0001 C CNN
	1    9550 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 1900 9550 1900
Wire Wire Line
	9550 2150 9550 2100
Connection ~ 9550 1900
Wire Wire Line
	9700 2300 9700 2100
Wire Wire Line
	9700 2100 9550 2100
Connection ~ 9550 2100
Wire Wire Line
	9550 2100 9550 1900
Wire Wire Line
	9550 2450 8750 2450
Wire Wire Line
	8750 2000 8750 2450
Connection ~ 8750 2450
$Comp
L Device:C C41
U 1 1 60A97DA9
P 7600 1800
F 0 "C41" V 7348 1800 50  0000 C CNN
F 1 "0.22uF" V 7439 1800 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D8.0mm_W5.0mm_P5.00mm" H 7638 1650 50  0001 C CNN
F 3 "~" H 7600 1800 50  0001 C CNN
	1    7600 1800
	0    1    1    0   
$EndComp
$Comp
L Device:R R61
U 1 1 60A99568
P 7750 2100
F 0 "R61" H 7820 2146 50  0000 L CNN
F 1 "1M" H 7820 2055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7680 2100 50  0001 C CNN
F 3 "~" H 7750 2100 50  0001 C CNN
	1    7750 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1800 8750 1800
Wire Wire Line
	7750 1950 7750 1800
Connection ~ 7750 1800
Wire Wire Line
	7750 2250 7750 2800
Wire Wire Line
	7750 2800 8750 2800
Connection ~ 8750 2800
Wire Wire Line
	8750 2800 8750 2850
Text Notes 6750 1250 0    50   ~ 0
Remove DC offset (about 2 sec to reach 0) 
$Comp
L Device:C C42
U 1 1 60AC351F
P 7550 4300
F 0 "C42" V 7298 4300 50  0000 C CNN
F 1 "0.22uF" V 7389 4300 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D8.0mm_W5.0mm_P5.00mm" H 7588 4150 50  0001 C CNN
F 3 "~" H 7550 4300 50  0001 C CNN
	1    7550 4300
	0    1    1    0   
$EndComp
$Comp
L Device:R R64
U 1 1 60AC3525
P 7700 4600
F 0 "R64" H 7770 4646 50  0000 L CNN
F 1 "1M" H 7770 4555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7630 4600 50  0001 C CNN
F 3 "~" H 7700 4600 50  0001 C CNN
	1    7700 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 4300 8700 4300
Wire Wire Line
	7700 4450 7700 4300
Connection ~ 7700 4300
Wire Wire Line
	7700 4750 7700 5450
Wire Wire Line
	7700 5450 8700 5450
Connection ~ 8700 5450
Wire Wire Line
	6850 4300 7400 4300
Wire Wire Line
	6850 1800 7450 1800
Wire Wire Line
	9300 4400 9500 4400
$Comp
L Device:R_POT RV19
U 1 1 60AEE98C
P 9500 4800
F 0 "RV19" H 9430 4846 50  0000 R CNN
F 1 "10k" H 9430 4755 50  0000 R CNN
F 2 "Kosmo_panel:Potentiometer_Alpha_RD901F-40-00D_Single_Vertical_3D" H 9500 4800 50  0001 C CNN
F 3 "~" H 9500 4800 50  0001 C CNN
	1    9500 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 4650 9500 4600
Wire Wire Line
	9650 4800 9650 4600
Wire Wire Line
	9650 4600 9500 4600
Connection ~ 9500 4600
Wire Wire Line
	9500 4600 9500 4400
Wire Wire Line
	9500 4950 8700 4950
Wire Wire Line
	8700 4500 8700 4950
Connection ~ 9500 4400
Connection ~ 8700 4950
Wire Notes Line
	8500 5950 8500 1150
$Comp
L Device:C C43
U 1 1 608690A7
P 2050 6200
F 0 "C43" H 2165 6246 50  0000 L CNN
F 1 "0.1uF" H 2165 6155 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 2088 6050 50  0001 C CNN
F 3 "~" H 2050 6200 50  0001 C CNN
	1    2050 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 6050 2050 6050
$Comp
L Device:C C44
U 1 1 6086F28C
P 2050 6500
F 0 "C44" H 2165 6546 50  0000 L CNN
F 1 "0.1uF" H 2165 6455 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 2088 6350 50  0001 C CNN
F 3 "~" H 2050 6500 50  0001 C CNN
	1    2050 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 6650 2050 6650
$Comp
L power:GND #PWR085
U 1 1 6087236B
P 2450 6350
F 0 "#PWR085" H 2450 6100 50  0001 C CNN
F 1 "GND" H 2455 6177 50  0000 C CNN
F 2 "" H 2450 6350 50  0001 C CNN
F 3 "" H 2450 6350 50  0001 C CNN
	1    2450 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 6350 2450 6350
Connection ~ 2050 6350
Connection ~ 1550 6650
Connection ~ 1550 6050
Wire Wire Line
	9500 4400 10400 4400
Wire Wire Line
	9550 1900 10400 1900
Text Notes 9500 1600 0    50   ~ 0
The 1k resistor on the output are on\nthe breakout board.
Text HLabel 4950 4750 2    50   Input ~ 0
Line_In_L
Text HLabel 4950 3450 2    50   Input ~ 0
Line_In_R
$Comp
L power:GND #PWR?
U 1 1 609B8C84
P 2800 3850
AR Path="/60190F78/609B8C84" Ref="#PWR?"  Part="1" 
AR Path="/604EEE41/609B8C84" Ref="#PWR079"  Part="1" 
F 0 "#PWR079" H 2800 3600 50  0001 C CNN
F 1 "GND" H 2805 3677 50  0000 C CNN
F 2 "" H 2800 3850 50  0001 C CNN
F 3 "" H 2800 3850 50  0001 C CNN
	1    2800 3850
	1    0    0    -1  
$EndComp
Text HLabel 1900 3550 0    50   Input ~ 0
In_L
Text HLabel 1900 4850 0    50   Input ~ 0
In_R
$Comp
L Device:R_POT RV?
U 1 1 609B8C8C
P 2650 3700
AR Path="/60190F78/609B8C8C" Ref="RV?"  Part="1" 
AR Path="/604EEE41/609B8C8C" Ref="RV18"  Part="1" 
F 0 "RV18" H 2580 3746 50  0000 R CNN
F 1 "10k" H 2580 3655 50  0000 R CNN
F 2 "Kosmo_panel:Potentiometer_Alpha_RD901F-40-00D_Single_Vertical_3D" H 2650 3700 50  0001 C CNN
F 3 "~" H 2650 3700 50  0001 C CNN
	1    2650 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3850 2800 3850
Wire Wire Line
	2800 3850 2800 3700
$Comp
L power:GND #PWR?
U 1 1 609B8C94
P 2800 5150
AR Path="/60190F78/609B8C94" Ref="#PWR?"  Part="1" 
AR Path="/604EEE41/609B8C94" Ref="#PWR082"  Part="1" 
F 0 "#PWR082" H 2800 4900 50  0001 C CNN
F 1 "GND" H 2805 4977 50  0000 C CNN
F 2 "" H 2800 5150 50  0001 C CNN
F 3 "" H 2800 5150 50  0001 C CNN
	1    2800 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV?
U 1 1 609B8C9A
P 2650 5000
AR Path="/60190F78/609B8C9A" Ref="RV?"  Part="1" 
AR Path="/604EEE41/609B8C9A" Ref="RV20"  Part="1" 
F 0 "RV20" H 2580 5046 50  0000 R CNN
F 1 "10k" H 2580 4955 50  0000 R CNN
F 2 "Kosmo_panel:Potentiometer_Alpha_RD901F-40-00D_Single_Vertical_3D" H 2650 5000 50  0001 C CNN
F 3 "~" H 2650 5000 50  0001 C CNN
	1    2650 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 5150 2800 5150
Wire Wire Line
	2800 5150 2800 5000
Text Notes 800  2850 0    50   ~ 0
The SGTL5000 has a maximum input range of\n-1.6V - 1.6V for the linein. This range can be completed\nused by opening the pot (about 10k to GND), but we\ncan also attenuate stronger signals. The values are\nbased on the 29k input impedance of the SGTL5000.\n\nThe Zener diodes have a Zener voltage of 13.75 and thus\nclamp the signal to +/-1.75V. The NZX14B has very low\nleakage currentof 50nA, which makes for a voltage\ndifference of 1.45mV (29k load by SGTL5000). That is\nless than 0.1% error.\n\nThe PZU14B2A,115 is the SMD version with\nslightly higher leakage (100nA).\n\nInfo about SGTL5000:\nhttps://forum.arduino.cc/t/audio-input-analyzed-w-arduino-fft/345446/35
Connection ~ 2800 3850
Connection ~ 2800 5150
$Comp
L Device:D_Zener D?
U 1 1 609B8CA5
P 4300 3300
AR Path="/60190F78/609B8CA5" Ref="D?"  Part="1" 
AR Path="/604EEE41/609B8CA5" Ref="D12"  Part="1" 
F 0 "D12" V 4254 3380 50  0000 L CNN
F 1 "NZX14B,133" V 4345 3380 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4300 3300 50  0001 C CNN
F 3 "~" H 4300 3300 50  0001 C CNN
	1    4300 3300
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 609B8CAB
P 4300 3150
AR Path="/60190F78/609B8CAB" Ref="#PWR?"  Part="1" 
AR Path="/604EEE41/609B8CAB" Ref="#PWR077"  Part="1" 
F 0 "#PWR077" H 4300 3000 50  0001 C CNN
F 1 "+12V" H 4315 3323 50  0000 C CNN
F 2 "" H 4300 3150 50  0001 C CNN
F 3 "" H 4300 3150 50  0001 C CNN
	1    4300 3150
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 609B8CB1
P 5050 3750
AR Path="/60190F78/609B8CB1" Ref="#PWR?"  Part="1" 
AR Path="/604EEE41/609B8CB1" Ref="#PWR078"  Part="1" 
F 0 "#PWR078" H 5050 3850 50  0001 C CNN
F 1 "-12V" H 5065 3923 50  0000 C CNN
F 2 "" H 5050 3750 50  0001 C CNN
F 3 "" H 5050 3750 50  0001 C CNN
	1    5050 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D?
U 1 1 609B8CB7
P 4300 3600
AR Path="/60190F78/609B8CB7" Ref="D?"  Part="1" 
AR Path="/604EEE41/609B8CB7" Ref="D13"  Part="1" 
F 0 "D13" V 4254 3680 50  0000 L CNN
F 1 "NZX14B,133" V 4345 3680 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4300 3600 50  0001 C CNN
F 3 "~" H 4300 3600 50  0001 C CNN
	1    4300 3600
	0    1    1    0   
$EndComp
Connection ~ 4300 3450
Wire Wire Line
	4300 4750 4050 4750
$Comp
L Device:D_Zener D?
U 1 1 609B8CBF
P 4300 4600
AR Path="/60190F78/609B8CBF" Ref="D?"  Part="1" 
AR Path="/604EEE41/609B8CBF" Ref="D14"  Part="1" 
F 0 "D14" V 4254 4680 50  0000 L CNN
F 1 "NZX14B,133" V 4345 4680 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4300 4600 50  0001 C CNN
F 3 "~" H 4300 4600 50  0001 C CNN
	1    4300 4600
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 609B8CC5
P 4300 4450
AR Path="/60190F78/609B8CC5" Ref="#PWR?"  Part="1" 
AR Path="/604EEE41/609B8CC5" Ref="#PWR080"  Part="1" 
F 0 "#PWR080" H 4300 4300 50  0001 C CNN
F 1 "+12V" H 4315 4623 50  0000 C CNN
F 2 "" H 4300 4450 50  0001 C CNN
F 3 "" H 4300 4450 50  0001 C CNN
	1    4300 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D?
U 1 1 609B8CCB
P 4300 4900
AR Path="/60190F78/609B8CCB" Ref="D?"  Part="1" 
AR Path="/604EEE41/609B8CCB" Ref="D15"  Part="1" 
F 0 "D15" V 4254 4980 50  0000 L CNN
F 1 "NZX14B,133" V 4345 4980 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4300 4900 50  0001 C CNN
F 3 "~" H 4300 4900 50  0001 C CNN
	1    4300 4900
	0    1    1    0   
$EndComp
Connection ~ 4300 4750
Wire Wire Line
	4300 3450 4950 3450
Wire Wire Line
	4300 3750 5050 3750
$Comp
L power:-12V #PWR?
U 1 1 609B8CD4
P 5050 5050
AR Path="/60190F78/609B8CD4" Ref="#PWR?"  Part="1" 
AR Path="/604EEE41/609B8CD4" Ref="#PWR081"  Part="1" 
F 0 "#PWR081" H 5050 5150 50  0001 C CNN
F 1 "-12V" H 5065 5223 50  0000 C CNN
F 2 "" H 5050 5050 50  0001 C CNN
F 3 "" H 5050 5050 50  0001 C CNN
	1    5050 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 5050 4300 5050
Wire Wire Line
	4300 4750 4950 4750
Wire Wire Line
	3050 3350 3050 3000
Wire Wire Line
	3050 3000 3650 3000
Wire Wire Line
	2650 3550 3050 3550
Connection ~ 2650 3550
$Comp
L Device:R R?
U 1 1 609B8CEC
P 3900 3450
AR Path="/60190F78/609B8CEC" Ref="R?"  Part="1" 
AR Path="/604EEE41/609B8CEC" Ref="R63"  Part="1" 
F 0 "R63" V 3693 3450 50  0000 C CNN
F 1 "3.3k" V 3784 3450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3830 3450 50  0001 C CNN
F 3 "~" H 3900 3450 50  0001 C CNN
	1    3900 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 3450 3650 3000
Wire Wire Line
	3650 3450 3750 3450
Connection ~ 3650 3450
Wire Wire Line
	4050 3450 4300 3450
Wire Wire Line
	2650 4850 3050 4850
Connection ~ 2650 4850
Wire Wire Line
	3050 4650 3050 4300
Wire Wire Line
	3050 4300 3650 4300
Wire Wire Line
	3650 4300 3650 4750
$Comp
L Device:R R?
U 1 1 609B8CFB
P 3900 4750
AR Path="/60190F78/609B8CFB" Ref="R?"  Part="1" 
AR Path="/604EEE41/609B8CFB" Ref="R65"  Part="1" 
F 0 "R65" V 3693 4750 50  0000 C CNN
F 1 "3.3k" V 3784 4750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3830 4750 50  0001 C CNN
F 3 "~" H 3900 4750 50  0001 C CNN
	1    3900 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 4750 3750 4750
Connection ~ 3650 4750
Wire Wire Line
	1900 3550 2650 3550
Wire Wire Line
	1900 4850 2650 4850
Text Notes 600  3350 0    50   ~ 0
The 20k resistors on the input are on the\nbreakout board.
Wire Notes Line
	5700 500  5700 7750
Text Notes 2000 4850 0    50   ~ 0
20kΩ breakout
Text Notes 2000 3550 0    50   ~ 0
20kΩ breakout
Text Notes 9750 1900 0    50   ~ 0
1kΩ breakout
Text Notes 9750 4400 0    50   ~ 0
1kΩ breakout
$Comp
L Amplifier_Operational:TL074 U?
U 1 1 609B8CE2
P 3350 4750
AR Path="/60190F78/609B8CE2" Ref="U?"  Part="2" 
AR Path="/604EEE41/609B8CE2" Ref="U9"  Part="1" 
F 0 "U9" H 3350 4383 50  0000 C CNN
F 1 "TL074" H 3350 4474 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 3300 4850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 3400 4950 50  0001 C CNN
	1    3350 4750
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:TL074 U9
U 4 1 60B64125
P 9050 1900
F 0 "U9" H 9050 2267 50  0000 C CNN
F 1 "TL074" H 9050 2176 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 9000 2000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 9100 2100 50  0001 C CNN
	4    9050 1900
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL074 U9
U 3 1 60B657C4
P 9000 4400
F 0 "U9" H 9000 4767 50  0000 C CNN
F 1 "TL074" H 9000 4676 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 8950 4500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 9050 4600 50  0001 C CNN
	3    9000 4400
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL074 U9
U 5 1 60B7BF83
P 1650 6350
F 0 "U9" H 1608 6396 50  0000 L CNN
F 1 "TL074" H 1608 6305 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 1600 6450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 1700 6550 50  0001 C CNN
	5    1650 6350
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL074 U?
U 2 1 609B8CDC
P 3350 3450
AR Path="/60190F78/609B8CDC" Ref="U?"  Part="1" 
AR Path="/604EEE41/609B8CDC" Ref="U9"  Part="2" 
F 0 "U9" H 3350 3083 50  0000 C CNN
F 1 "TL074" H 3350 3174 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 3300 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 3400 3650 50  0001 C CNN
	2    3350 3450
	1    0    0    1   
$EndComp
$EndSCHEMATC
