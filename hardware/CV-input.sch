EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 9
Title "Teen Kosmology"
Date ""
Rev "0.2"
Comp "HB"
Comment1 "UNTESTED UNTESTED UNTESTED UNTESTED"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 600849A0
P 4150 3300
AR Path="/6007C274/600849A0" Ref="R?"  Part="1" 
AR Path="/600880AD/600849A0" Ref="R?"  Part="1" 
AR Path="/6008ABB3/600849A0" Ref="R?"  Part="1" 
AR Path="/6008ACF5/600849A0" Ref="R?"  Part="1" 
AR Path="/6008AD9F/600849A0" Ref="R?"  Part="1" 
AR Path="/6008AE67/600849A0" Ref="R?"  Part="1" 
AR Path="/6008BFD9/600849A0" Ref="R?"  Part="1" 
AR Path="/6008C203/600849A0" Ref="R?"  Part="1" 
AR Path="/6009C38B/600849A0" Ref="R?"  Part="1" 
AR Path="/6009C458/600849A0" Ref="R?"  Part="1" 
AR Path="/6009C548/600849A0" Ref="R?"  Part="1" 
AR Path="/600849A0" Ref="R?"  Part="1" 
AR Path="/6015C780/6017339C/600849A0" Ref="R8"  Part="1" 
AR Path="/6015C780/601733A2/600849A0" Ref="R22"  Part="1" 
AR Path="/6015C780/601733A6/600849A0" Ref="R36"  Part="1" 
AR Path="/6015C780/601733AA/600849A0" Ref="R50"  Part="1" 
F 0 "R22" V 3943 3300 50  0000 C CNN
F 1 "1.2k" V 4034 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4080 3300 50  0001 C CNN
F 3 "~" H 4150 3300 50  0001 C CNN
	1    4150 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 600849A6
P 4650 3300
AR Path="/6007C274/600849A6" Ref="R?"  Part="1" 
AR Path="/600880AD/600849A6" Ref="R?"  Part="1" 
AR Path="/6008ABB3/600849A6" Ref="R?"  Part="1" 
AR Path="/6008ACF5/600849A6" Ref="R?"  Part="1" 
AR Path="/6008AD9F/600849A6" Ref="R?"  Part="1" 
AR Path="/6008AE67/600849A6" Ref="R?"  Part="1" 
AR Path="/6008BFD9/600849A6" Ref="R?"  Part="1" 
AR Path="/6008C203/600849A6" Ref="R?"  Part="1" 
AR Path="/6009C38B/600849A6" Ref="R?"  Part="1" 
AR Path="/6009C458/600849A6" Ref="R?"  Part="1" 
AR Path="/6009C548/600849A6" Ref="R?"  Part="1" 
AR Path="/600849A6" Ref="R?"  Part="1" 
AR Path="/6015C780/6017339C/600849A6" Ref="R9"  Part="1" 
AR Path="/6015C780/601733A2/600849A6" Ref="R23"  Part="1" 
AR Path="/6015C780/601733A6/600849A6" Ref="R37"  Part="1" 
AR Path="/6015C780/601733AA/600849A6" Ref="R51"  Part="1" 
F 0 "R23" V 4443 3300 50  0000 C CNN
F 1 "56k" V 4534 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4580 3300 50  0001 C CNN
F 3 "~" H 4650 3300 50  0001 C CNN
	1    4650 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 600849AC
P 5150 3300
AR Path="/6007C274/600849AC" Ref="R?"  Part="1" 
AR Path="/600880AD/600849AC" Ref="R?"  Part="1" 
AR Path="/6008ABB3/600849AC" Ref="R?"  Part="1" 
AR Path="/6008ACF5/600849AC" Ref="R?"  Part="1" 
AR Path="/6008AD9F/600849AC" Ref="R?"  Part="1" 
AR Path="/6008AE67/600849AC" Ref="R?"  Part="1" 
AR Path="/6008BFD9/600849AC" Ref="R?"  Part="1" 
AR Path="/6008C203/600849AC" Ref="R?"  Part="1" 
AR Path="/6009C38B/600849AC" Ref="R?"  Part="1" 
AR Path="/6009C458/600849AC" Ref="R?"  Part="1" 
AR Path="/6009C548/600849AC" Ref="R?"  Part="1" 
AR Path="/600849AC" Ref="R?"  Part="1" 
AR Path="/6015C780/6017339C/600849AC" Ref="R10"  Part="1" 
AR Path="/6015C780/601733A2/600849AC" Ref="R24"  Part="1" 
AR Path="/6015C780/601733A6/600849AC" Ref="R38"  Part="1" 
AR Path="/6015C780/601733AA/600849AC" Ref="R52"  Part="1" 
F 0 "R24" V 4943 3300 50  0000 C CNN
F 1 "150k" V 5034 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5080 3300 50  0001 C CNN
F 3 "~" H 5150 3300 50  0001 C CNN
	1    5150 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 3300 4400 3300
$Comp
L Device:C C?
U 1 1 600849B3
P 4400 3750
AR Path="/6007C274/600849B3" Ref="C?"  Part="1" 
AR Path="/600880AD/600849B3" Ref="C?"  Part="1" 
AR Path="/6008ABB3/600849B3" Ref="C?"  Part="1" 
AR Path="/6008ACF5/600849B3" Ref="C?"  Part="1" 
AR Path="/6008AD9F/600849B3" Ref="C?"  Part="1" 
AR Path="/6008AE67/600849B3" Ref="C?"  Part="1" 
AR Path="/6008BFD9/600849B3" Ref="C?"  Part="1" 
AR Path="/6008C203/600849B3" Ref="C?"  Part="1" 
AR Path="/6009C38B/600849B3" Ref="C?"  Part="1" 
AR Path="/6009C458/600849B3" Ref="C?"  Part="1" 
AR Path="/6009C548/600849B3" Ref="C?"  Part="1" 
AR Path="/600849B3" Ref="C?"  Part="1" 
AR Path="/6015C780/6017339C/600849B3" Ref="C10"  Part="1" 
AR Path="/6015C780/601733A2/600849B3" Ref="C18"  Part="1" 
AR Path="/6015C780/601733A6/600849B3" Ref="C26"  Part="1" 
AR Path="/6015C780/601733AA/600849B3" Ref="C34"  Part="1" 
F 0 "C18" H 4515 3796 50  0000 L CNN
F 1 "6.8nF" H 4515 3705 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 4438 3600 50  0001 C CNN
F 3 "~" H 4400 3750 50  0001 C CNN
	1    4400 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 600849BF
P 4900 3750
AR Path="/6007C274/600849BF" Ref="C?"  Part="1" 
AR Path="/600880AD/600849BF" Ref="C?"  Part="1" 
AR Path="/6008ABB3/600849BF" Ref="C?"  Part="1" 
AR Path="/6008ACF5/600849BF" Ref="C?"  Part="1" 
AR Path="/6008AD9F/600849BF" Ref="C?"  Part="1" 
AR Path="/6008AE67/600849BF" Ref="C?"  Part="1" 
AR Path="/6008BFD9/600849BF" Ref="C?"  Part="1" 
AR Path="/6008C203/600849BF" Ref="C?"  Part="1" 
AR Path="/6009C38B/600849BF" Ref="C?"  Part="1" 
AR Path="/6009C458/600849BF" Ref="C?"  Part="1" 
AR Path="/6009C548/600849BF" Ref="C?"  Part="1" 
AR Path="/600849BF" Ref="C?"  Part="1" 
AR Path="/6015C780/6017339C/600849BF" Ref="C11"  Part="1" 
AR Path="/6015C780/601733A2/600849BF" Ref="C19"  Part="1" 
AR Path="/6015C780/601733A6/600849BF" Ref="C27"  Part="1" 
AR Path="/6015C780/601733AA/600849BF" Ref="C35"  Part="1" 
F 0 "C19" H 5015 3796 50  0000 L CNN
F 1 "330pF" H 5015 3705 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 4938 3600 50  0001 C CNN
F 3 "~" H 4900 3750 50  0001 C CNN
	1    4900 3750
	1    0    0    -1  
$EndComp
Connection ~ 4400 3300
Wire Wire Line
	4400 3300 4500 3300
Wire Wire Line
	4400 3300 4400 3600
Wire Wire Line
	4800 3300 4900 3300
Connection ~ 4900 3300
Wire Wire Line
	4900 3300 5000 3300
Wire Wire Line
	4900 3300 4900 2550
$Comp
L power:GND #PWR?
U 1 1 600849D1
P 5550 4300
AR Path="/6007C274/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/600880AD/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/6008ABB3/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/6008ACF5/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/6008AD9F/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/6008AE67/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/6008BFD9/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/6008C203/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/6009C38B/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/6009C458/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/6009C548/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/600849D1" Ref="#PWR?"  Part="1" 
AR Path="/6015C780/6017339C/600849D1" Ref="#PWR037"  Part="1" 
AR Path="/6015C780/601733A2/600849D1" Ref="#PWR048"  Part="1" 
AR Path="/6015C780/601733A6/600849D1" Ref="#PWR059"  Part="1" 
AR Path="/6015C780/601733AA/600849D1" Ref="#PWR070"  Part="1" 
F 0 "#PWR059" H 5550 4050 50  0001 C CNN
F 1 "GND" H 5555 4127 50  0000 C CNN
F 2 "" H 5550 4300 50  0001 C CNN
F 3 "" H 5550 4300 50  0001 C CNN
	1    5550 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3900 4400 4150
Wire Wire Line
	6400 2550 6400 3400
$Comp
L Device:R R?
U 1 1 60084A0F
P 6900 3400
AR Path="/6007C274/60084A0F" Ref="R?"  Part="1" 
AR Path="/600880AD/60084A0F" Ref="R?"  Part="1" 
AR Path="/6008ABB3/60084A0F" Ref="R?"  Part="1" 
AR Path="/6008ACF5/60084A0F" Ref="R?"  Part="1" 
AR Path="/6008AD9F/60084A0F" Ref="R?"  Part="1" 
AR Path="/6008AE67/60084A0F" Ref="R?"  Part="1" 
AR Path="/6008BFD9/60084A0F" Ref="R?"  Part="1" 
AR Path="/6008C203/60084A0F" Ref="R?"  Part="1" 
AR Path="/6009C38B/60084A0F" Ref="R?"  Part="1" 
AR Path="/6009C458/60084A0F" Ref="R?"  Part="1" 
AR Path="/6009C548/60084A0F" Ref="R?"  Part="1" 
AR Path="/60084A0F" Ref="R?"  Part="1" 
AR Path="/6015C780/6017339C/60084A0F" Ref="R11"  Part="1" 
AR Path="/6015C780/601733A2/60084A0F" Ref="R25"  Part="1" 
AR Path="/6015C780/601733A6/60084A0F" Ref="R39"  Part="1" 
AR Path="/6015C780/601733AA/60084A0F" Ref="R53"  Part="1" 
F 0 "R25" V 6693 3400 50  0000 C CNN
F 1 "4.7k" V 6784 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6830 3400 50  0001 C CNN
F 3 "~" H 6900 3400 50  0001 C CNN
	1    6900 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	3750 3300 3800 3300
Text HLabel 8000 3400 2    50   Output ~ 0
ADC1
Text HLabel 8000 5650 2    50   Output ~ 0
ADC2
$Comp
L power:+12V #PWR?
U 1 1 60082D0A
P 1000 4000
AR Path="/6009C38B/60082D0A" Ref="#PWR?"  Part="1" 
AR Path="/6009C458/60082D0A" Ref="#PWR?"  Part="1" 
AR Path="/6009C548/60082D0A" Ref="#PWR?"  Part="1" 
AR Path="/6007C274/60082D0A" Ref="#PWR?"  Part="1" 
AR Path="/6015C780/6017339C/60082D0A" Ref="#PWR035"  Part="1" 
AR Path="/6015C780/601733A2/60082D0A" Ref="#PWR046"  Part="1" 
AR Path="/6015C780/601733A6/60082D0A" Ref="#PWR057"  Part="1" 
AR Path="/6015C780/601733AA/60082D0A" Ref="#PWR068"  Part="1" 
F 0 "#PWR057" H 1000 3850 50  0001 C CNN
F 1 "+12V" H 1015 4173 50  0000 C CNN
F 2 "" H 1000 4000 50  0001 C CNN
F 3 "" H 1000 4000 50  0001 C CNN
	1    1000 4000
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 600835A7
P 1000 4600
AR Path="/6009C38B/600835A7" Ref="#PWR?"  Part="1" 
AR Path="/6009C458/600835A7" Ref="#PWR?"  Part="1" 
AR Path="/6009C548/600835A7" Ref="#PWR?"  Part="1" 
AR Path="/6007C274/600835A7" Ref="#PWR?"  Part="1" 
AR Path="/6015C780/6017339C/600835A7" Ref="#PWR038"  Part="1" 
AR Path="/6015C780/601733A2/600835A7" Ref="#PWR049"  Part="1" 
AR Path="/6015C780/601733A6/600835A7" Ref="#PWR060"  Part="1" 
AR Path="/6015C780/601733AA/600835A7" Ref="#PWR071"  Part="1" 
F 0 "#PWR060" H 1000 4700 50  0001 C CNN
F 1 "-12V" H 1015 4773 50  0000 C CNN
F 2 "" H 1000 4600 50  0001 C CNN
F 3 "" H 1000 4600 50  0001 C CNN
	1    1000 4600
	-1   0    0    1   
$EndComp
Text HLabel 1250 3200 0    50   Input ~ 0
CV1
Text HLabel 1250 5450 0    50   Input ~ 0
CV2
Text Notes 1450 1600 0    50   ~ 0
Attenuation to about -2.5V to +2.5V of -10V to +10V inputs\nwhen pot is at about 2/3, and voltage shifting to half the ADC\nreference voltag by using an inverting summer.\n\nNote that the 20k resistors on the input before the pots\nare on the breakout board and thus only indicated here\nwith a label.
Text Notes 4250 2000 0    50   ~ 0
3 pole Butterworth filter (1 pole + 2 pole MFB)\nwith 25kHz cuttoff frequency.\n\nThe values can be changed, if the filter is found to be too\naggressive. But with watch out for aliasing if the cut-off\nfrequency is raised. This is a handy tool to experiment\nwith different values:\nhttp://sim.okawa-denshi.jp/en/MultipleFB3Lowkeisan.htm
Text Notes 6800 2150 0    50   ~ 0
Clamping between 5.5V and -0.5V (5V and GND with diode drop).\nThis should be fine because the 4.7k resistor limits the excess\ncurrent to about 100μA, which the MCP308 should be able to\nhandle.\n\nNote that any other silicon diode pair can do the job, only that\nthe BAV99 is quite a handy package and has very low reverse\ncurrent. Also, Schottky diodes could be used, but they are even\nworse in introducing noise even though they provide a tighter\nclamp!
$Comp
L Device:R_POT RV?
U 1 1 600849EE
P 2350 3200
AR Path="/6007C274/600849EE" Ref="RV?"  Part="1" 
AR Path="/600880AD/600849EE" Ref="RV?"  Part="1" 
AR Path="/6008ABB3/600849EE" Ref="RV?"  Part="1" 
AR Path="/6008ACF5/600849EE" Ref="RV?"  Part="1" 
AR Path="/6008AD9F/600849EE" Ref="RV?"  Part="1" 
AR Path="/6008AE67/600849EE" Ref="RV?"  Part="1" 
AR Path="/6008BFD9/600849EE" Ref="RV?"  Part="1" 
AR Path="/6008C203/600849EE" Ref="RV?"  Part="1" 
AR Path="/6009C38B/600849EE" Ref="RV?"  Part="1" 
AR Path="/6009C458/600849EE" Ref="RV?"  Part="1" 
AR Path="/6009C548/600849EE" Ref="RV?"  Part="1" 
AR Path="/600849EE" Ref="RV?"  Part="1" 
AR Path="/6015C780/6017339C/600849EE" Ref="RV9"  Part="1" 
AR Path="/6015C780/601733A2/600849EE" Ref="RV11"  Part="1" 
AR Path="/6015C780/601733A6/600849EE" Ref="RV13"  Part="1" 
AR Path="/6015C780/601733AA/600849EE" Ref="RV15"  Part="1" 
F 0 "RV11" H 2281 3246 50  0000 R CNN
F 1 "100k" H 2281 3155 50  0000 R CNN
F 2 "Kosmo_panel:Potentiometer_Alpha_RD901F-40-00D_Single_Vertical_3D" H 2350 3200 50  0001 C CNN
F 3 "~" H 2350 3200 50  0001 C CNN
	1    2350 3200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R6
U 1 1 6063184B
P 3450 2800
AR Path="/6015C780/6017339C/6063184B" Ref="R6"  Part="1" 
AR Path="/6015C780/601733A2/6063184B" Ref="R20"  Part="1" 
AR Path="/6015C780/601733A6/6063184B" Ref="R34"  Part="1" 
AR Path="/6015C780/601733AA/6063184B" Ref="R48"  Part="1" 
F 0 "R20" V 3243 2800 50  0000 C CNN
F 1 "10k" V 3334 2800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3380 2800 50  0001 C CNN
F 3 "~" H 3450 2800 50  0001 C CNN
	1    3450 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	2350 3050 2150 3050
Wire Wire Line
	2150 3050 2150 3200
Connection ~ 2150 3200
Wire Wire Line
	2150 3200 2200 3200
$Comp
L Device:R R7
U 1 1 60657CA3
P 2800 2850
AR Path="/6015C780/6017339C/60657CA3" Ref="R7"  Part="1" 
AR Path="/6015C780/601733A2/60657CA3" Ref="R21"  Part="1" 
AR Path="/6015C780/601733A6/60657CA3" Ref="R35"  Part="1" 
AR Path="/6015C780/601733AA/60657CA3" Ref="R49"  Part="1" 
F 0 "R21" H 2870 2896 50  0000 L CNN
F 1 "10k" H 2870 2805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2730 2850 50  0001 C CNN
F 3 "~" H 2800 2850 50  0001 C CNN
	1    2800 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4150 4400 4150
Connection ~ 4400 4150
$Comp
L Device:R R5
U 1 1 606AFE55
P 5150 2550
AR Path="/6015C780/6017339C/606AFE55" Ref="R5"  Part="1" 
AR Path="/6015C780/601733A2/606AFE55" Ref="R19"  Part="1" 
AR Path="/6015C780/601733A6/606AFE55" Ref="R33"  Part="1" 
AR Path="/6015C780/601733AA/606AFE55" Ref="R47"  Part="1" 
F 0 "R19" V 4943 2550 50  0000 C CNN
F 1 "56k" V 5034 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5080 2550 50  0001 C CNN
F 3 "~" H 5150 2550 50  0001 C CNN
	1    5150 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 2550 4900 2550
Wire Wire Line
	5300 2550 5450 2550
Wire Wire Line
	5550 4300 5550 4150
Connection ~ 5550 4150
Wire Wire Line
	5300 3300 5450 3300
Wire Wire Line
	4400 4150 4900 4150
Wire Wire Line
	4900 3300 4900 3600
Wire Wire Line
	4900 3900 4900 4150
Connection ~ 4900 4150
Wire Wire Line
	4900 4150 5550 4150
Wire Wire Line
	5450 2550 5450 2700
Connection ~ 5450 2550
Wire Wire Line
	5450 2550 6400 2550
Wire Wire Line
	5450 3000 5450 3300
Connection ~ 5450 3300
Wire Wire Line
	5450 3300 5800 3300
Wire Wire Line
	2800 2550 2800 2700
Wire Wire Line
	2500 3200 2800 3200
Wire Wire Line
	2800 3000 2800 3200
Connection ~ 2800 3200
Wire Wire Line
	2800 3200 3100 3200
Text Notes 750  950  0    79   ~ 16
This module prepares two bipolar CV inputs to be fed into an ADC\nwith input range of 0-5V and a sampling frequency of 100kHz.
$Comp
L Device:C C9
U 1 1 6074DE93
P 5450 2850
AR Path="/6015C780/6017339C/6074DE93" Ref="C9"  Part="1" 
AR Path="/6015C780/601733A2/6074DE93" Ref="C17"  Part="1" 
AR Path="/6015C780/601733A6/6074DE93" Ref="C25"  Part="1" 
AR Path="/6015C780/601733AA/6074DE93" Ref="C33"  Part="1" 
F 0 "C17" H 5565 2896 50  0000 L CNN
F 1 "22pF" H 5565 2805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5488 2700 50  0001 C CNN
F 3 "~" H 5450 2850 50  0001 C CNN
	1    5450 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4150 5700 4150
Wire Wire Line
	5800 3500 5700 3500
Wire Wire Line
	5700 3500 5700 4150
Wire Wire Line
	6400 3400 6750 3400
Wire Wire Line
	3150 3400 3000 3400
Wire Wire Line
	3000 3400 3000 4150
Wire Wire Line
	3100 3200 3100 2800
Wire Wire Line
	3100 2800 3300 2800
Connection ~ 3100 3200
Wire Wire Line
	3100 3200 3150 3200
Wire Wire Line
	3600 2800 3800 2800
Wire Wire Line
	3800 2800 3800 3300
Connection ~ 3800 3300
Wire Wire Line
	3800 3300 4000 3300
$Comp
L Diode:BAV99 D4
U 1 1 6075F9A3
P 7200 3000
AR Path="/6015C780/6017339C/6075F9A3" Ref="D4"  Part="1" 
AR Path="/6015C780/601733A2/6075F9A3" Ref="D6"  Part="1" 
AR Path="/6015C780/601733A6/6075F9A3" Ref="D8"  Part="1" 
AR Path="/6015C780/601733AA/6075F9A3" Ref="D10"  Part="1" 
F 0 "D6" H 7200 3216 50  0000 C CNN
F 1 "BAV99" H 7200 3125 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7200 2500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV99_SER.pdf" H 7200 3000 50  0001 C CNN
	1    7200 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR033
U 1 1 607616E9
P 6900 3000
AR Path="/6015C780/6017339C/607616E9" Ref="#PWR033"  Part="1" 
AR Path="/6015C780/601733A2/607616E9" Ref="#PWR044"  Part="1" 
AR Path="/6015C780/601733A6/607616E9" Ref="#PWR055"  Part="1" 
AR Path="/6015C780/601733AA/607616E9" Ref="#PWR066"  Part="1" 
F 0 "#PWR055" H 6900 2750 50  0001 C CNN
F 1 "GND" H 6750 2900 50  0000 C CNN
F 2 "" H 6900 3000 50  0001 C CNN
F 3 "" H 6900 3000 50  0001 C CNN
	1    6900 3000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR034
U 1 1 60762151
P 7500 3000
AR Path="/6015C780/6017339C/60762151" Ref="#PWR034"  Part="1" 
AR Path="/6015C780/601733A2/60762151" Ref="#PWR045"  Part="1" 
AR Path="/6015C780/601733A6/60762151" Ref="#PWR056"  Part="1" 
AR Path="/6015C780/601733AA/60762151" Ref="#PWR067"  Part="1" 
F 0 "#PWR056" H 7500 2850 50  0001 C CNN
F 1 "+5V" H 7515 3173 50  0000 C CNN
F 2 "" H 7500 3000 50  0001 C CNN
F 3 "" H 7500 3000 50  0001 C CNN
	1    7500 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3400 7200 3400
Connection ~ 7200 3400
Wire Wire Line
	7200 3400 8000 3400
Wire Wire Line
	7200 3200 7200 3400
$Comp
L Device:R R?
U 1 1 60888ABB
P 4150 5550
AR Path="/6007C274/60888ABB" Ref="R?"  Part="1" 
AR Path="/600880AD/60888ABB" Ref="R?"  Part="1" 
AR Path="/6008ABB3/60888ABB" Ref="R?"  Part="1" 
AR Path="/6008ACF5/60888ABB" Ref="R?"  Part="1" 
AR Path="/6008AD9F/60888ABB" Ref="R?"  Part="1" 
AR Path="/6008AE67/60888ABB" Ref="R?"  Part="1" 
AR Path="/6008BFD9/60888ABB" Ref="R?"  Part="1" 
AR Path="/6008C203/60888ABB" Ref="R?"  Part="1" 
AR Path="/6009C38B/60888ABB" Ref="R?"  Part="1" 
AR Path="/6009C458/60888ABB" Ref="R?"  Part="1" 
AR Path="/6009C548/60888ABB" Ref="R?"  Part="1" 
AR Path="/60888ABB" Ref="R?"  Part="1" 
AR Path="/6015C780/6017339C/60888ABB" Ref="R15"  Part="1" 
AR Path="/6015C780/601733A2/60888ABB" Ref="R29"  Part="1" 
AR Path="/6015C780/601733A6/60888ABB" Ref="R43"  Part="1" 
AR Path="/6015C780/601733AA/60888ABB" Ref="R57"  Part="1" 
F 0 "R29" V 3943 5550 50  0000 C CNN
F 1 "1.2k" V 4034 5550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4080 5550 50  0001 C CNN
F 3 "~" H 4150 5550 50  0001 C CNN
	1    4150 5550
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 60888AC1
P 4650 5550
AR Path="/6007C274/60888AC1" Ref="R?"  Part="1" 
AR Path="/600880AD/60888AC1" Ref="R?"  Part="1" 
AR Path="/6008ABB3/60888AC1" Ref="R?"  Part="1" 
AR Path="/6008ACF5/60888AC1" Ref="R?"  Part="1" 
AR Path="/6008AD9F/60888AC1" Ref="R?"  Part="1" 
AR Path="/6008AE67/60888AC1" Ref="R?"  Part="1" 
AR Path="/6008BFD9/60888AC1" Ref="R?"  Part="1" 
AR Path="/6008C203/60888AC1" Ref="R?"  Part="1" 
AR Path="/6009C38B/60888AC1" Ref="R?"  Part="1" 
AR Path="/6009C458/60888AC1" Ref="R?"  Part="1" 
AR Path="/6009C548/60888AC1" Ref="R?"  Part="1" 
AR Path="/60888AC1" Ref="R?"  Part="1" 
AR Path="/6015C780/6017339C/60888AC1" Ref="R16"  Part="1" 
AR Path="/6015C780/601733A2/60888AC1" Ref="R30"  Part="1" 
AR Path="/6015C780/601733A6/60888AC1" Ref="R44"  Part="1" 
AR Path="/6015C780/601733AA/60888AC1" Ref="R58"  Part="1" 
F 0 "R30" V 4443 5550 50  0000 C CNN
F 1 "56k" V 4534 5550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4580 5550 50  0001 C CNN
F 3 "~" H 4650 5550 50  0001 C CNN
	1    4650 5550
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 60888AC7
P 5150 5550
AR Path="/6007C274/60888AC7" Ref="R?"  Part="1" 
AR Path="/600880AD/60888AC7" Ref="R?"  Part="1" 
AR Path="/6008ABB3/60888AC7" Ref="R?"  Part="1" 
AR Path="/6008ACF5/60888AC7" Ref="R?"  Part="1" 
AR Path="/6008AD9F/60888AC7" Ref="R?"  Part="1" 
AR Path="/6008AE67/60888AC7" Ref="R?"  Part="1" 
AR Path="/6008BFD9/60888AC7" Ref="R?"  Part="1" 
AR Path="/6008C203/60888AC7" Ref="R?"  Part="1" 
AR Path="/6009C38B/60888AC7" Ref="R?"  Part="1" 
AR Path="/6009C458/60888AC7" Ref="R?"  Part="1" 
AR Path="/6009C548/60888AC7" Ref="R?"  Part="1" 
AR Path="/60888AC7" Ref="R?"  Part="1" 
AR Path="/6015C780/6017339C/60888AC7" Ref="R17"  Part="1" 
AR Path="/6015C780/601733A2/60888AC7" Ref="R31"  Part="1" 
AR Path="/6015C780/601733A6/60888AC7" Ref="R45"  Part="1" 
AR Path="/6015C780/601733AA/60888AC7" Ref="R59"  Part="1" 
F 0 "R31" V 4943 5550 50  0000 C CNN
F 1 "150k" V 5034 5550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5080 5550 50  0001 C CNN
F 3 "~" H 5150 5550 50  0001 C CNN
	1    5150 5550
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 5550 4400 5550
$Comp
L Device:C C?
U 1 1 60888ACE
P 4400 6000
AR Path="/6007C274/60888ACE" Ref="C?"  Part="1" 
AR Path="/600880AD/60888ACE" Ref="C?"  Part="1" 
AR Path="/6008ABB3/60888ACE" Ref="C?"  Part="1" 
AR Path="/6008ACF5/60888ACE" Ref="C?"  Part="1" 
AR Path="/6008AD9F/60888ACE" Ref="C?"  Part="1" 
AR Path="/6008AE67/60888ACE" Ref="C?"  Part="1" 
AR Path="/6008BFD9/60888ACE" Ref="C?"  Part="1" 
AR Path="/6008C203/60888ACE" Ref="C?"  Part="1" 
AR Path="/6009C38B/60888ACE" Ref="C?"  Part="1" 
AR Path="/6009C458/60888ACE" Ref="C?"  Part="1" 
AR Path="/6009C548/60888ACE" Ref="C?"  Part="1" 
AR Path="/60888ACE" Ref="C?"  Part="1" 
AR Path="/6015C780/6017339C/60888ACE" Ref="C15"  Part="1" 
AR Path="/6015C780/601733A2/60888ACE" Ref="C23"  Part="1" 
AR Path="/6015C780/601733A6/60888ACE" Ref="C31"  Part="1" 
AR Path="/6015C780/601733AA/60888ACE" Ref="C39"  Part="1" 
F 0 "C23" H 4515 6046 50  0000 L CNN
F 1 "6.8nF" H 4515 5955 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 4438 5850 50  0001 C CNN
F 3 "~" H 4400 6000 50  0001 C CNN
	1    4400 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60888AD4
P 4900 6000
AR Path="/6007C274/60888AD4" Ref="C?"  Part="1" 
AR Path="/600880AD/60888AD4" Ref="C?"  Part="1" 
AR Path="/6008ABB3/60888AD4" Ref="C?"  Part="1" 
AR Path="/6008ACF5/60888AD4" Ref="C?"  Part="1" 
AR Path="/6008AD9F/60888AD4" Ref="C?"  Part="1" 
AR Path="/6008AE67/60888AD4" Ref="C?"  Part="1" 
AR Path="/6008BFD9/60888AD4" Ref="C?"  Part="1" 
AR Path="/6008C203/60888AD4" Ref="C?"  Part="1" 
AR Path="/6009C38B/60888AD4" Ref="C?"  Part="1" 
AR Path="/6009C458/60888AD4" Ref="C?"  Part="1" 
AR Path="/6009C548/60888AD4" Ref="C?"  Part="1" 
AR Path="/60888AD4" Ref="C?"  Part="1" 
AR Path="/6015C780/6017339C/60888AD4" Ref="C16"  Part="1" 
AR Path="/6015C780/601733A2/60888AD4" Ref="C24"  Part="1" 
AR Path="/6015C780/601733A6/60888AD4" Ref="C32"  Part="1" 
AR Path="/6015C780/601733AA/60888AD4" Ref="C40"  Part="1" 
F 0 "C24" H 5015 6046 50  0000 L CNN
F 1 "330pF" H 5015 5955 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 4938 5850 50  0001 C CNN
F 3 "~" H 4900 6000 50  0001 C CNN
	1    4900 6000
	1    0    0    -1  
$EndComp
Connection ~ 4400 5550
Wire Wire Line
	4400 5550 4500 5550
Wire Wire Line
	4400 5550 4400 5850
Wire Wire Line
	4800 5550 4900 5550
Connection ~ 4900 5550
Wire Wire Line
	4900 5550 5000 5550
Wire Wire Line
	4900 5550 4900 4800
$Comp
L power:GND #PWR?
U 1 1 60888AE1
P 5550 6550
AR Path="/6007C274/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/600880AD/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/6008ABB3/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/6008ACF5/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/6008AD9F/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/6008AE67/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/6008BFD9/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/6008C203/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/6009C38B/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/6009C458/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/6009C548/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/60888AE1" Ref="#PWR?"  Part="1" 
AR Path="/6015C780/6017339C/60888AE1" Ref="#PWR042"  Part="1" 
AR Path="/6015C780/601733A2/60888AE1" Ref="#PWR053"  Part="1" 
AR Path="/6015C780/601733A6/60888AE1" Ref="#PWR064"  Part="1" 
AR Path="/6015C780/601733AA/60888AE1" Ref="#PWR075"  Part="1" 
F 0 "#PWR064" H 5550 6300 50  0001 C CNN
F 1 "GND" H 5555 6377 50  0000 C CNN
F 2 "" H 5550 6550 50  0001 C CNN
F 3 "" H 5550 6550 50  0001 C CNN
	1    5550 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 6150 4400 6400
Wire Wire Line
	6400 4800 6400 5650
$Comp
L Device:R R?
U 1 1 60888AF5
P 6900 5650
AR Path="/6007C274/60888AF5" Ref="R?"  Part="1" 
AR Path="/600880AD/60888AF5" Ref="R?"  Part="1" 
AR Path="/6008ABB3/60888AF5" Ref="R?"  Part="1" 
AR Path="/6008ACF5/60888AF5" Ref="R?"  Part="1" 
AR Path="/6008AD9F/60888AF5" Ref="R?"  Part="1" 
AR Path="/6008AE67/60888AF5" Ref="R?"  Part="1" 
AR Path="/6008BFD9/60888AF5" Ref="R?"  Part="1" 
AR Path="/6008C203/60888AF5" Ref="R?"  Part="1" 
AR Path="/6009C38B/60888AF5" Ref="R?"  Part="1" 
AR Path="/6009C458/60888AF5" Ref="R?"  Part="1" 
AR Path="/6009C548/60888AF5" Ref="R?"  Part="1" 
AR Path="/60888AF5" Ref="R?"  Part="1" 
AR Path="/6015C780/6017339C/60888AF5" Ref="R18"  Part="1" 
AR Path="/6015C780/601733A2/60888AF5" Ref="R32"  Part="1" 
AR Path="/6015C780/601733A6/60888AF5" Ref="R46"  Part="1" 
AR Path="/6015C780/601733AA/60888AF5" Ref="R60"  Part="1" 
F 0 "R32" V 6693 5650 50  0000 C CNN
F 1 "4.7k" V 6784 5650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6830 5650 50  0001 C CNN
F 3 "~" H 6900 5650 50  0001 C CNN
	1    6900 5650
	0    1    1    0   
$EndComp
Wire Wire Line
	3750 5550 3800 5550
$Comp
L Device:R_POT RV?
U 1 1 60888AFF
P 2350 5450
AR Path="/6007C274/60888AFF" Ref="RV?"  Part="1" 
AR Path="/600880AD/60888AFF" Ref="RV?"  Part="1" 
AR Path="/6008ABB3/60888AFF" Ref="RV?"  Part="1" 
AR Path="/6008ACF5/60888AFF" Ref="RV?"  Part="1" 
AR Path="/6008AD9F/60888AFF" Ref="RV?"  Part="1" 
AR Path="/6008AE67/60888AFF" Ref="RV?"  Part="1" 
AR Path="/6008BFD9/60888AFF" Ref="RV?"  Part="1" 
AR Path="/6008C203/60888AFF" Ref="RV?"  Part="1" 
AR Path="/6009C38B/60888AFF" Ref="RV?"  Part="1" 
AR Path="/6009C458/60888AFF" Ref="RV?"  Part="1" 
AR Path="/6009C548/60888AFF" Ref="RV?"  Part="1" 
AR Path="/60888AFF" Ref="RV?"  Part="1" 
AR Path="/6015C780/6017339C/60888AFF" Ref="RV10"  Part="1" 
AR Path="/6015C780/601733A2/60888AFF" Ref="RV12"  Part="1" 
AR Path="/6015C780/601733A6/60888AFF" Ref="RV14"  Part="1" 
AR Path="/6015C780/601733AA/60888AFF" Ref="RV16"  Part="1" 
F 0 "RV12" H 2281 5496 50  0000 R CNN
F 1 "100k" H 2281 5405 50  0000 R CNN
F 2 "Kosmo_panel:Potentiometer_Alpha_RD901F-40-00D_Single_Vertical_3D" H 2350 5450 50  0001 C CNN
F 3 "~" H 2350 5450 50  0001 C CNN
	1    2350 5450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R13
U 1 1 60888B07
P 3450 5050
AR Path="/6015C780/6017339C/60888B07" Ref="R13"  Part="1" 
AR Path="/6015C780/601733A2/60888B07" Ref="R27"  Part="1" 
AR Path="/6015C780/601733A6/60888B07" Ref="R41"  Part="1" 
AR Path="/6015C780/601733AA/60888B07" Ref="R55"  Part="1" 
F 0 "R27" V 3243 5050 50  0000 C CNN
F 1 "10k" V 3334 5050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3380 5050 50  0001 C CNN
F 3 "~" H 3450 5050 50  0001 C CNN
	1    3450 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	2350 5300 2150 5300
Wire Wire Line
	2150 5300 2150 5450
Connection ~ 2150 5450
Wire Wire Line
	2150 5450 2200 5450
$Comp
L Device:R R14
U 1 1 60888B11
P 2800 5100
AR Path="/6015C780/6017339C/60888B11" Ref="R14"  Part="1" 
AR Path="/6015C780/601733A2/60888B11" Ref="R28"  Part="1" 
AR Path="/6015C780/601733A6/60888B11" Ref="R42"  Part="1" 
AR Path="/6015C780/601733AA/60888B11" Ref="R56"  Part="1" 
F 0 "R28" H 2870 5146 50  0000 L CNN
F 1 "10k" H 2870 5055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2730 5100 50  0001 C CNN
F 3 "~" H 2800 5100 50  0001 C CNN
	1    2800 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 6400 4400 6400
Connection ~ 4400 6400
$Comp
L Device:R R12
U 1 1 60888B19
P 5150 4800
AR Path="/6015C780/6017339C/60888B19" Ref="R12"  Part="1" 
AR Path="/6015C780/601733A2/60888B19" Ref="R26"  Part="1" 
AR Path="/6015C780/601733A6/60888B19" Ref="R40"  Part="1" 
AR Path="/6015C780/601733AA/60888B19" Ref="R54"  Part="1" 
F 0 "R26" V 4943 4800 50  0000 C CNN
F 1 "56k" V 5034 4800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5080 4800 50  0001 C CNN
F 3 "~" H 5150 4800 50  0001 C CNN
	1    5150 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 4800 4900 4800
Wire Wire Line
	5300 4800 5450 4800
Wire Wire Line
	5550 6550 5550 6400
Connection ~ 5550 6400
Wire Wire Line
	5300 5550 5450 5550
Wire Wire Line
	4400 6400 4900 6400
Wire Wire Line
	4900 5550 4900 5850
Wire Wire Line
	4900 6150 4900 6400
Connection ~ 4900 6400
Wire Wire Line
	4900 6400 5550 6400
Wire Wire Line
	5450 4800 5450 4950
Connection ~ 5450 4800
Wire Wire Line
	5450 4800 6400 4800
Wire Wire Line
	5450 5250 5450 5550
Connection ~ 5450 5550
Wire Wire Line
	5450 5550 5800 5550
Wire Wire Line
	2800 4800 2800 4950
Wire Wire Line
	2500 5450 2800 5450
Wire Wire Line
	2800 5250 2800 5450
Connection ~ 2800 5450
Wire Wire Line
	2800 5450 3100 5450
$Comp
L Device:C C14
U 1 1 60888B34
P 5450 5100
AR Path="/6015C780/6017339C/60888B34" Ref="C14"  Part="1" 
AR Path="/6015C780/601733A2/60888B34" Ref="C22"  Part="1" 
AR Path="/6015C780/601733A6/60888B34" Ref="C30"  Part="1" 
AR Path="/6015C780/601733AA/60888B34" Ref="C38"  Part="1" 
F 0 "C22" H 5565 5146 50  0000 L CNN
F 1 "22pF" H 5565 5055 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5488 4950 50  0001 C CNN
F 3 "~" H 5450 5100 50  0001 C CNN
	1    5450 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 6400 5700 6400
Wire Wire Line
	5800 5750 5700 5750
Wire Wire Line
	5700 5750 5700 6400
Wire Wire Line
	6400 5650 6750 5650
Wire Wire Line
	3150 5650 3000 5650
Wire Wire Line
	3000 5650 3000 6400
Wire Wire Line
	3100 5450 3100 5050
Wire Wire Line
	3100 5050 3300 5050
Connection ~ 3100 5450
Wire Wire Line
	3100 5450 3150 5450
Wire Wire Line
	3600 5050 3800 5050
Wire Wire Line
	3800 5050 3800 5550
Connection ~ 3800 5550
Wire Wire Line
	3800 5550 4000 5550
$Comp
L Diode:BAV99 D5
U 1 1 60888B49
P 7200 5250
AR Path="/6015C780/6017339C/60888B49" Ref="D5"  Part="1" 
AR Path="/6015C780/601733A2/60888B49" Ref="D7"  Part="1" 
AR Path="/6015C780/601733A6/60888B49" Ref="D9"  Part="1" 
AR Path="/6015C780/601733AA/60888B49" Ref="D11"  Part="1" 
F 0 "D7" H 7200 5466 50  0000 C CNN
F 1 "BAV99" H 7200 5375 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7200 4750 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV99_SER.pdf" H 7200 5250 50  0001 C CNN
	1    7200 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR040
U 1 1 60888B4F
P 6900 5250
AR Path="/6015C780/6017339C/60888B4F" Ref="#PWR040"  Part="1" 
AR Path="/6015C780/601733A2/60888B4F" Ref="#PWR051"  Part="1" 
AR Path="/6015C780/601733A6/60888B4F" Ref="#PWR062"  Part="1" 
AR Path="/6015C780/601733AA/60888B4F" Ref="#PWR073"  Part="1" 
F 0 "#PWR062" H 6900 5000 50  0001 C CNN
F 1 "GND" H 6750 5150 50  0000 C CNN
F 2 "" H 6900 5250 50  0001 C CNN
F 3 "" H 6900 5250 50  0001 C CNN
	1    6900 5250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR041
U 1 1 60888B55
P 7500 5250
AR Path="/6015C780/6017339C/60888B55" Ref="#PWR041"  Part="1" 
AR Path="/6015C780/601733A2/60888B55" Ref="#PWR052"  Part="1" 
AR Path="/6015C780/601733A6/60888B55" Ref="#PWR063"  Part="1" 
AR Path="/6015C780/601733AA/60888B55" Ref="#PWR074"  Part="1" 
F 0 "#PWR063" H 7500 5100 50  0001 C CNN
F 1 "+5V" H 7515 5423 50  0000 C CNN
F 2 "" H 7500 5250 50  0001 C CNN
F 3 "" H 7500 5250 50  0001 C CNN
	1    7500 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 5650 7200 5650
Connection ~ 7200 5650
Wire Wire Line
	7200 5650 8000 5650
Wire Wire Line
	7200 5450 7200 5650
Wire Notes Line
	4000 1300 4000 7300
Wire Notes Line
	6650 1250 6650 7300
$Comp
L power:+2V5 #PWR032
U 1 1 6115FEE0
P 2800 2550
AR Path="/6015C780/6017339C/6115FEE0" Ref="#PWR032"  Part="1" 
AR Path="/6015C780/601733A2/6115FEE0" Ref="#PWR043"  Part="1" 
AR Path="/6015C780/601733A6/6115FEE0" Ref="#PWR054"  Part="1" 
AR Path="/6015C780/601733AA/6115FEE0" Ref="#PWR065"  Part="1" 
F 0 "#PWR054" H 2800 2400 50  0001 C CNN
F 1 "+2V5" H 2815 2723 50  0000 C CNN
F 2 "" H 2800 2550 50  0001 C CNN
F 3 "" H 2800 2550 50  0001 C CNN
	1    2800 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+2V5 #PWR039
U 1 1 61164694
P 2800 4800
AR Path="/6015C780/6017339C/61164694" Ref="#PWR039"  Part="1" 
AR Path="/6015C780/601733A2/61164694" Ref="#PWR050"  Part="1" 
AR Path="/6015C780/601733A6/61164694" Ref="#PWR061"  Part="1" 
AR Path="/6015C780/601733AA/61164694" Ref="#PWR072"  Part="1" 
F 0 "#PWR061" H 2800 4650 50  0001 C CNN
F 1 "+2V5" H 2815 4973 50  0000 C CNN
F 2 "" H 2800 4800 50  0001 C CNN
F 3 "" H 2800 4800 50  0001 C CNN
	1    2800 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 6081E2AF
P 1400 4000
AR Path="/6015C780/6017339C/6081E2AF" Ref="C12"  Part="1" 
AR Path="/6015C780/601733AA/6081E2AF" Ref="C36"  Part="1" 
AR Path="/6015C780/601733A2/6081E2AF" Ref="C20"  Part="1" 
AR Path="/6015C780/601733A6/6081E2AF" Ref="C28"  Part="1" 
F 0 "C20" V 1148 4000 50  0000 C CNN
F 1 "0.1uF" V 1239 4000 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 1438 3850 50  0001 C CNN
F 3 "~" H 1400 4000 50  0001 C CNN
	1    1400 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	1000 4000 1250 4000
$Comp
L power:GND #PWR036
U 1 1 6082CEE0
P 1850 4300
AR Path="/6015C780/6017339C/6082CEE0" Ref="#PWR036"  Part="1" 
AR Path="/6015C780/601733AA/6082CEE0" Ref="#PWR069"  Part="1" 
AR Path="/6015C780/601733A2/6082CEE0" Ref="#PWR047"  Part="1" 
AR Path="/6015C780/601733A6/6082CEE0" Ref="#PWR058"  Part="1" 
F 0 "#PWR058" H 1850 4050 50  0001 C CNN
F 1 "GND" H 1855 4127 50  0000 C CNN
F 2 "" H 1850 4300 50  0001 C CNN
F 3 "" H 1850 4300 50  0001 C CNN
	1    1850 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 6082D132
P 1400 4600
AR Path="/6015C780/6017339C/6082D132" Ref="C13"  Part="1" 
AR Path="/6015C780/601733AA/6082D132" Ref="C37"  Part="1" 
AR Path="/6015C780/601733A2/6082D132" Ref="C21"  Part="1" 
AR Path="/6015C780/601733A6/6082D132" Ref="C29"  Part="1" 
F 0 "C21" V 1148 4600 50  0000 C CNN
F 1 "0.1uF" V 1239 4600 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 1438 4450 50  0001 C CNN
F 3 "~" H 1400 4600 50  0001 C CNN
	1    1400 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	1000 4600 1250 4600
Wire Wire Line
	1550 4000 1550 4300
Wire Wire Line
	1550 4300 1850 4300
Wire Wire Line
	1550 4600 1550 4300
Connection ~ 1550 4300
Wire Wire Line
	1250 3200 2150 3200
Wire Wire Line
	1250 5450 2150 5450
Connection ~ 6400 5650
Connection ~ 1000 4600
Connection ~ 1000 4000
Text Notes 1400 3200 0    50   ~ 0
20kΩ breakout
Text Notes 1400 5450 0    50   ~ 0
20kΩ breakout
$Comp
L Amplifier_Operational:TL074 U7
U 4 1 6092967A
P 6100 5650
AR Path="/6015C780/601733A6/6092967A" Ref="U7"  Part="4" 
AR Path="/6015C780/6017339C/6092967A" Ref="U5"  Part="4" 
AR Path="/6015C780/601733A2/6092967A" Ref="U6"  Part="4" 
AR Path="/6015C780/601733AA/6092967A" Ref="U8"  Part="4" 
F 0 "U6" H 6100 5283 50  0000 C CNN
F 1 "TL074" H 6100 5374 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 6050 5750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 6150 5850 50  0001 C CNN
	4    6100 5650
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:TL074 U7
U 5 1 6092C4EF
P 1100 4300
AR Path="/6015C780/601733A6/6092C4EF" Ref="U7"  Part="5" 
AR Path="/6015C780/6017339C/6092C4EF" Ref="U5"  Part="5" 
AR Path="/6015C780/601733A2/6092C4EF" Ref="U6"  Part="5" 
AR Path="/6015C780/601733AA/6092C4EF" Ref="U8"  Part="5" 
F 0 "U6" H 1058 4346 50  0000 L CNN
F 1 "TL074" H 1058 4255 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 1050 4400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 1150 4500 50  0001 C CNN
	5    1100 4300
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL074 U7
U 3 1 60928267
P 3450 5550
AR Path="/6015C780/601733A6/60928267" Ref="U7"  Part="3" 
AR Path="/6015C780/6017339C/60928267" Ref="U5"  Part="3" 
AR Path="/6015C780/601733A2/60928267" Ref="U6"  Part="3" 
AR Path="/6015C780/601733AA/60928267" Ref="U8"  Part="3" 
F 0 "U6" H 3450 5183 50  0000 C CNN
F 1 "TL074" H 3450 5274 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 3400 5650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 3500 5750 50  0001 C CNN
	3    3450 5550
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:TL074 U5
U 2 1 609EA6D3
P 3450 3300
AR Path="/6015C780/6017339C/609EA6D3" Ref="U5"  Part="2" 
AR Path="/6015C780/601733AA/609EA6D3" Ref="U8"  Part="2" 
AR Path="/6015C780/601733A2/609EA6D3" Ref="U6"  Part="2" 
AR Path="/6015C780/601733A6/609EA6D3" Ref="U7"  Part="2" 
F 0 "U6" H 3450 2933 50  0000 C CNN
F 1 "TL074" H 3450 3024 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 3400 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 3500 3500 50  0001 C CNN
	2    3450 3300
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:TL074 U5
U 1 1 609EBE8A
P 6100 3400
AR Path="/6015C780/6017339C/609EBE8A" Ref="U5"  Part="1" 
AR Path="/6015C780/601733AA/609EBE8A" Ref="U8"  Part="1" 
AR Path="/6015C780/601733A2/609EBE8A" Ref="U6"  Part="1" 
AR Path="/6015C780/601733A6/609EBE8A" Ref="U7"  Part="1" 
F 0 "U6" H 6100 3033 50  0000 C CNN
F 1 "TL074" H 6100 3124 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 6050 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 6150 3600 50  0001 C CNN
	1    6100 3400
	1    0    0    1   
$EndComp
Connection ~ 6400 3400
$EndSCHEMATC
