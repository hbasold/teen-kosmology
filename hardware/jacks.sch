EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 13 13
Title ""
Date ""
Rev "0.2"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 1300 2650
NoConn ~ 1300 2450
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B29A35
P 1100 2350
AR Path="/6009C38B/60B29A35" Ref="J?"  Part="1" 
AR Path="/6009C458/60B29A35" Ref="J?"  Part="1" 
AR Path="/6007C274/60B29A35" Ref="J?"  Part="1" 
AR Path="/60B29A35" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B29A35" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B29A35" Ref="J8"  Part="1" 
F 0 "J8" H 1082 2675 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 1082 2584 50  0000 C CNN
F 2 "" H 1100 2350 50  0001 C CNN
F 3 "~" H 1100 2350 50  0001 C CNN
	1    1100 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 2250 1300 2250
$Comp
L power:GND1 #PWR076
U 1 1 60B2BD83
P 1650 2250
F 0 "#PWR076" H 1650 2000 50  0001 C CNN
F 1 "GND1" H 1655 2077 50  0000 C CNN
F 2 "" H 1650 2250 50  0001 C CNN
F 3 "" H 1650 2250 50  0001 C CNN
	1    1650 2250
	1    0    0    -1  
$EndComp
NoConn ~ 1300 2350
Text GLabel 1300 2550 2    50   Input ~ 0
Out_L_Breakout
NoConn ~ 2700 2650
NoConn ~ 2700 2450
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B2E9EC
P 2500 2350
AR Path="/6009C38B/60B2E9EC" Ref="J?"  Part="1" 
AR Path="/6009C458/60B2E9EC" Ref="J?"  Part="1" 
AR Path="/6007C274/60B2E9EC" Ref="J?"  Part="1" 
AR Path="/60B2E9EC" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B2E9EC" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B2E9EC" Ref="J9"  Part="1" 
F 0 "J9" H 2482 2675 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 2482 2584 50  0000 C CNN
F 2 "" H 2500 2350 50  0001 C CNN
F 3 "~" H 2500 2350 50  0001 C CNN
	1    2500 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2250 2700 2250
$Comp
L power:GND1 #PWR077
U 1 1 60B2E9F3
P 3050 2250
F 0 "#PWR077" H 3050 2000 50  0001 C CNN
F 1 "GND1" H 3055 2077 50  0000 C CNN
F 2 "" H 3050 2250 50  0001 C CNN
F 3 "" H 3050 2250 50  0001 C CNN
	1    3050 2250
	1    0    0    -1  
$EndComp
NoConn ~ 2700 2350
Text GLabel 2700 2550 2    50   Input ~ 0
Out_R_Breakout
NoConn ~ 1300 1500
NoConn ~ 1300 1300
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B3384C
P 1100 1200
AR Path="/6009C38B/60B3384C" Ref="J?"  Part="1" 
AR Path="/6009C458/60B3384C" Ref="J?"  Part="1" 
AR Path="/6007C274/60B3384C" Ref="J?"  Part="1" 
AR Path="/60B3384C" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B3384C" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B3384C" Ref="J6"  Part="1" 
F 0 "J6" H 1082 1525 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 1082 1434 50  0000 C CNN
F 2 "" H 1100 1200 50  0001 C CNN
F 3 "~" H 1100 1200 50  0001 C CNN
	1    1100 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1100 1300 1100
$Comp
L power:GND1 #PWR074
U 1 1 60B33853
P 1650 1100
F 0 "#PWR074" H 1650 850 50  0001 C CNN
F 1 "GND1" H 1655 927 50  0000 C CNN
F 2 "" H 1650 1100 50  0001 C CNN
F 3 "" H 1650 1100 50  0001 C CNN
	1    1650 1100
	1    0    0    -1  
$EndComp
NoConn ~ 1300 1200
Text GLabel 1300 1400 2    50   Output ~ 0
In_L_Breakout
NoConn ~ 2700 1500
NoConn ~ 2700 1300
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B3385D
P 2500 1200
AR Path="/6009C38B/60B3385D" Ref="J?"  Part="1" 
AR Path="/6009C458/60B3385D" Ref="J?"  Part="1" 
AR Path="/6007C274/60B3385D" Ref="J?"  Part="1" 
AR Path="/60B3385D" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B3385D" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B3385D" Ref="J7"  Part="1" 
F 0 "J7" H 2482 1525 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 2482 1434 50  0000 C CNN
F 2 "" H 2500 1200 50  0001 C CNN
F 3 "~" H 2500 1200 50  0001 C CNN
	1    2500 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 1100 2700 1100
$Comp
L power:GND1 #PWR075
U 1 1 60B33864
P 3050 1100
F 0 "#PWR075" H 3050 850 50  0001 C CNN
F 1 "GND1" H 3055 927 50  0000 C CNN
F 2 "" H 3050 1100 50  0001 C CNN
F 3 "" H 3050 1100 50  0001 C CNN
	1    3050 1100
	1    0    0    -1  
$EndComp
NoConn ~ 2700 1200
Text GLabel 2700 1400 2    50   Output ~ 0
In_R_Breakout
Wire Notes Line
	3850 2950 450  2950
NoConn ~ 1300 3750
NoConn ~ 1300 3550
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B4A22F
P 1100 3450
AR Path="/6009C38B/60B4A22F" Ref="J?"  Part="1" 
AR Path="/6009C458/60B4A22F" Ref="J?"  Part="1" 
AR Path="/6007C274/60B4A22F" Ref="J?"  Part="1" 
AR Path="/60B4A22F" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B4A22F" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B4A22F" Ref="J12"  Part="1" 
F 0 "J12" H 1082 3775 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 1082 3684 50  0000 C CNN
F 2 "" H 1100 3450 50  0001 C CNN
F 3 "~" H 1100 3450 50  0001 C CNN
	1    1100 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3350 1300 3350
$Comp
L power:GND1 #PWR082
U 1 1 60B4A236
P 1650 3350
F 0 "#PWR082" H 1650 3100 50  0001 C CNN
F 1 "GND1" H 1655 3177 50  0000 C CNN
F 2 "" H 1650 3350 50  0001 C CNN
F 3 "" H 1650 3350 50  0001 C CNN
	1    1650 3350
	1    0    0    -1  
$EndComp
NoConn ~ 1300 3450
Text GLabel 1300 3650 2    50   Output ~ 0
CV1_Breakout
NoConn ~ 2700 3750
NoConn ~ 2700 3550
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B4C067
P 2500 3450
AR Path="/6009C38B/60B4C067" Ref="J?"  Part="1" 
AR Path="/6009C458/60B4C067" Ref="J?"  Part="1" 
AR Path="/6007C274/60B4C067" Ref="J?"  Part="1" 
AR Path="/60B4C067" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B4C067" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B4C067" Ref="J13"  Part="1" 
F 0 "J13" H 2482 3775 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 2482 3684 50  0000 C CNN
F 2 "" H 2500 3450 50  0001 C CNN
F 3 "~" H 2500 3450 50  0001 C CNN
	1    2500 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3350 2700 3350
$Comp
L power:GND1 #PWR083
U 1 1 60B4C06E
P 3050 3350
F 0 "#PWR083" H 3050 3100 50  0001 C CNN
F 1 "GND1" H 3055 3177 50  0000 C CNN
F 2 "" H 3050 3350 50  0001 C CNN
F 3 "" H 3050 3350 50  0001 C CNN
	1    3050 3350
	1    0    0    -1  
$EndComp
NoConn ~ 2700 3450
Text GLabel 2700 3650 2    50   Output ~ 0
CV2_Breakout
NoConn ~ 1300 4600
NoConn ~ 1300 4400
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B55477
P 1100 4300
AR Path="/6009C38B/60B55477" Ref="J?"  Part="1" 
AR Path="/6009C458/60B55477" Ref="J?"  Part="1" 
AR Path="/6007C274/60B55477" Ref="J?"  Part="1" 
AR Path="/60B55477" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B55477" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B55477" Ref="J14"  Part="1" 
F 0 "J14" H 1082 4625 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 1082 4534 50  0000 C CNN
F 2 "" H 1100 4300 50  0001 C CNN
F 3 "~" H 1100 4300 50  0001 C CNN
	1    1100 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 4200 1300 4200
$Comp
L power:GND1 #PWR084
U 1 1 60B5547E
P 1650 4200
F 0 "#PWR084" H 1650 3950 50  0001 C CNN
F 1 "GND1" H 1655 4027 50  0000 C CNN
F 2 "" H 1650 4200 50  0001 C CNN
F 3 "" H 1650 4200 50  0001 C CNN
	1    1650 4200
	1    0    0    -1  
$EndComp
NoConn ~ 1300 4300
Text GLabel 1300 4500 2    50   Output ~ 0
CV3_Breakout
NoConn ~ 2700 4600
NoConn ~ 2700 4400
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B55488
P 2500 4300
AR Path="/6009C38B/60B55488" Ref="J?"  Part="1" 
AR Path="/6009C458/60B55488" Ref="J?"  Part="1" 
AR Path="/6007C274/60B55488" Ref="J?"  Part="1" 
AR Path="/60B55488" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B55488" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B55488" Ref="J15"  Part="1" 
F 0 "J15" H 2482 4625 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 2482 4534 50  0000 C CNN
F 2 "" H 2500 4300 50  0001 C CNN
F 3 "~" H 2500 4300 50  0001 C CNN
	1    2500 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4200 2700 4200
$Comp
L power:GND1 #PWR085
U 1 1 60B5548F
P 3050 4200
F 0 "#PWR085" H 3050 3950 50  0001 C CNN
F 1 "GND1" H 3055 4027 50  0000 C CNN
F 2 "" H 3050 4200 50  0001 C CNN
F 3 "" H 3050 4200 50  0001 C CNN
	1    3050 4200
	1    0    0    -1  
$EndComp
NoConn ~ 2700 4300
Text GLabel 2700 4500 2    50   Output ~ 0
CV4_Breakout
NoConn ~ 1300 5450
NoConn ~ 1300 5250
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B56D25
P 1100 5150
AR Path="/6009C38B/60B56D25" Ref="J?"  Part="1" 
AR Path="/6009C458/60B56D25" Ref="J?"  Part="1" 
AR Path="/6007C274/60B56D25" Ref="J?"  Part="1" 
AR Path="/60B56D25" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B56D25" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B56D25" Ref="J16"  Part="1" 
F 0 "J16" H 1082 5475 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 1082 5384 50  0000 C CNN
F 2 "" H 1100 5150 50  0001 C CNN
F 3 "~" H 1100 5150 50  0001 C CNN
	1    1100 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 5050 1300 5050
$Comp
L power:GND1 #PWR086
U 1 1 60B56D2C
P 1650 5050
F 0 "#PWR086" H 1650 4800 50  0001 C CNN
F 1 "GND1" H 1655 4877 50  0000 C CNN
F 2 "" H 1650 5050 50  0001 C CNN
F 3 "" H 1650 5050 50  0001 C CNN
	1    1650 5050
	1    0    0    -1  
$EndComp
NoConn ~ 1300 5150
Text GLabel 1300 5350 2    50   Output ~ 0
CV5_Breakout
NoConn ~ 2700 5450
NoConn ~ 2700 5250
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B56D36
P 2500 5150
AR Path="/6009C38B/60B56D36" Ref="J?"  Part="1" 
AR Path="/6009C458/60B56D36" Ref="J?"  Part="1" 
AR Path="/6007C274/60B56D36" Ref="J?"  Part="1" 
AR Path="/60B56D36" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B56D36" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B56D36" Ref="J17"  Part="1" 
F 0 "J17" H 2482 5475 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 2482 5384 50  0000 C CNN
F 2 "" H 2500 5150 50  0001 C CNN
F 3 "~" H 2500 5150 50  0001 C CNN
	1    2500 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 5050 2700 5050
$Comp
L power:GND1 #PWR087
U 1 1 60B56D3D
P 3050 5050
F 0 "#PWR087" H 3050 4800 50  0001 C CNN
F 1 "GND1" H 3055 4877 50  0000 C CNN
F 2 "" H 3050 5050 50  0001 C CNN
F 3 "" H 3050 5050 50  0001 C CNN
	1    3050 5050
	1    0    0    -1  
$EndComp
NoConn ~ 2700 5150
Text GLabel 2700 5350 2    50   Output ~ 0
CV6_Breakout
NoConn ~ 1300 6350
NoConn ~ 1300 6150
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B57FB6
P 1100 6050
AR Path="/6009C38B/60B57FB6" Ref="J?"  Part="1" 
AR Path="/6009C458/60B57FB6" Ref="J?"  Part="1" 
AR Path="/6007C274/60B57FB6" Ref="J?"  Part="1" 
AR Path="/60B57FB6" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B57FB6" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B57FB6" Ref="J18"  Part="1" 
F 0 "J18" H 1082 6375 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 1082 6284 50  0000 C CNN
F 2 "" H 1100 6050 50  0001 C CNN
F 3 "~" H 1100 6050 50  0001 C CNN
	1    1100 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 5950 1300 5950
$Comp
L power:GND1 #PWR088
U 1 1 60B57FBD
P 1650 5950
F 0 "#PWR088" H 1650 5700 50  0001 C CNN
F 1 "GND1" H 1655 5777 50  0000 C CNN
F 2 "" H 1650 5950 50  0001 C CNN
F 3 "" H 1650 5950 50  0001 C CNN
	1    1650 5950
	1    0    0    -1  
$EndComp
NoConn ~ 1300 6050
Text GLabel 1300 6250 2    50   Output ~ 0
CV7_Breakout
NoConn ~ 2700 6350
NoConn ~ 2700 6150
$Comp
L Connector:AudioJack3_SwitchTR J?
U 1 1 60B57FC7
P 2500 6050
AR Path="/6009C38B/60B57FC7" Ref="J?"  Part="1" 
AR Path="/6009C458/60B57FC7" Ref="J?"  Part="1" 
AR Path="/6007C274/60B57FC7" Ref="J?"  Part="1" 
AR Path="/60B57FC7" Ref="J?"  Part="1" 
AR Path="/604EEE41/60B57FC7" Ref="J?"  Part="1" 
AR Path="/60B2848E/60B57FC7" Ref="J19"  Part="1" 
F 0 "J19" H 2482 6375 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 2482 6284 50  0000 C CNN
F 2 "" H 2500 6050 50  0001 C CNN
F 3 "~" H 2500 6050 50  0001 C CNN
	1    2500 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 5950 2700 5950
$Comp
L power:GND1 #PWR089
U 1 1 60B57FCE
P 3050 5950
F 0 "#PWR089" H 3050 5700 50  0001 C CNN
F 1 "GND1" H 3055 5777 50  0000 C CNN
F 2 "" H 3050 5950 50  0001 C CNN
F 3 "" H 3050 5950 50  0001 C CNN
	1    3050 5950
	1    0    0    -1  
$EndComp
NoConn ~ 2700 6050
Text GLabel 2700 6250 2    50   Output ~ 0
CV8_Breakout
Wire Notes Line
	3850 6850 500  6850
Wire Notes Line
	3850 500  3850 6850
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J10
U 1 1 60B64984
P 5450 3000
F 0 "J10" H 5500 3517 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 5500 3426 50  0000 C CNN
F 2 "" H 5450 3000 50  0001 C CNN
F 3 "~" H 5450 3000 50  0001 C CNN
	1    5450 3000
	1    0    0    -1  
$EndComp
Text GLabel 5250 2800 0    50   Output ~ 0
In_R_Breakout
Text GLabel 5750 2900 2    50   Input ~ 0
Out_R_Breakout
Text GLabel 5750 3400 2    50   Output ~ 0
CV2_Breakout
Text GLabel 5750 3300 2    50   Output ~ 0
CV4_Breakout
Text GLabel 5750 3200 2    50   Output ~ 0
CV6_Breakout
Text GLabel 5750 3100 2    50   Output ~ 0
CV8_Breakout
$Comp
L power:GND1 #PWR079
U 1 1 60B6E4D0
P 6550 3000
F 0 "#PWR079" H 6550 2750 50  0001 C CNN
F 1 "GND1" H 6555 2827 50  0000 C CNN
F 2 "" H 6550 3000 50  0001 C CNN
F 3 "" H 6550 3000 50  0001 C CNN
	1    6550 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3000 6550 3000
Wire Wire Line
	6550 3000 6550 2700
Wire Wire Line
	6550 2700 5750 2700
Connection ~ 6550 3000
Text GLabel 5250 2900 0    50   Output ~ 0
In_L_Breakout
Text GLabel 5750 2800 2    50   Input ~ 0
Out_L_Breakout
Text GLabel 5250 3100 0    50   Output ~ 0
CV1_Breakout
Text GLabel 5250 3200 0    50   Output ~ 0
CV3_Breakout
Text GLabel 5250 3300 0    50   Output ~ 0
CV5_Breakout
Text GLabel 5250 3400 0    50   Output ~ 0
CV7_Breakout
$Comp
L power:GND1 #PWR078
U 1 1 60B751CE
P 4350 3000
F 0 "#PWR078" H 4350 2750 50  0001 C CNN
F 1 "GND1" H 4355 2827 50  0000 C CNN
F 2 "" H 4350 3000 50  0001 C CNN
F 3 "" H 4350 3000 50  0001 C CNN
	1    4350 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3000 5250 3000
Wire Wire Line
	4350 3000 4350 2700
Wire Wire Line
	4350 2700 5250 2700
Connection ~ 4350 3000
$EndSCHEMATC
